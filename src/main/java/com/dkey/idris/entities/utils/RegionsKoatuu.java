package com.dkey.idris.entities.utils;

public enum RegionsKoatuu {

    CRM("01", "АР КРИМ"),
    VIN("05", "ВІННИЦЬКА"),
    VOL("07", "ВОЛИНСЬКА"),
    DNP("12", "ДНІПРОПЕТРОВСЬКА"),
    DON("14", "ДОНЕЦЬКА"),
    ZHI("18", "ЖИТОМИРСЬКА"),
    ZKP("21", "ЗАКАРПАТСЬКА"),
    ZPR("23", "ЗАПОРІЗЬКА"),
    IVF("26", "ІВАНО-ФРАНКІВСЬКА"),
    KIR("32", "КИЇВСЬКА"),
    KVG("35", "КІРОВОГРАДСЬКА"),
    LUG("44", "ЛУГАНСЬКА"),
    LVV("46", "ЛЬВІВСЬКА"),
    MIK("48", "МИКОЛАЇВСЬКА"),
    ODS("51", "ОДЕСЬКА"),
    POL("53", "ПОЛТАВСЬКА"),
    RIV("56", "РІВНЕНСЬКА"),
    SUM("59", "СУМСЬКА"),
    TER("61", "ТЕРНОПІЛЬСЬКА"),
    KHA("63", "ХАРКІВСЬКА"),
    KHE("65", "ХЕРСОНСЬКА"),
    KHM("68", "ХМЕЛЬНИЦЬКА"),
    CHK("71", "ЧЕРКАСЬКА"),
    CHV("73", "ЧЕРНІВЕЦЬКА"),
    CHN("74", "ЧЕРНІГІВСЬКА"),
    SEV("85", "СЕВАСТОПОЛЬ"),
    KIV("80", "КИЇВ"),
    UNKNOWN("00", "UNNOWN");

    private final String id;
    private final String value;

    private RegionsKoatuu(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public static String getValueById(String id) {
        for (RegionsKoatuu r : values() ) {
            if (r.id.equals(id)) {
                return r.value;
            }
        }
        return RegionsKoatuu.UNKNOWN.value;
    }

}
