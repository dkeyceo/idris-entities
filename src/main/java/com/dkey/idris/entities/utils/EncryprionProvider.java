package com.dkey.idris.entities.utils;

import org.apache.commons.codec.binary.Base64;

public class EncryprionProvider {

    public static String decrypt(String string){
        byte [] bytesEncoded = Base64.decodeBase64(string.getBytes());
        return new String(bytesEncoded);
    }
}
