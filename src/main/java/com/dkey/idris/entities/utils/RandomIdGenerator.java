package com.dkey.idris.entities.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.OptionalLong;
import java.util.Random;

public class RandomIdGenerator {


    private long getRandomNumber() {
        Random random = new Random();
        OptionalLong x = random.longs().findFirst();
        return  Math.abs(x.getAsLong());
    }

    public String getRandomId() {
        String pattern = "yyyyMMddHHmmssSSS";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        long randomData = getRandomNumber();
        return String.format("%s%d", sdf.format(new Date()), randomData);
    }
}
