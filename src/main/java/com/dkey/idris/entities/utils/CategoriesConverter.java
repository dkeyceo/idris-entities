package com.dkey.idris.entities.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Converter
public class CategoriesConverter implements AttributeConverter<List<Map<String, String>>, String> {

    private final Logger logger = LoggerFactory.getLogger(CategoriesConverter.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    public String convertToDatabaseColumn(List<Map<String, String>> customerInfo) {

    String customerInfoJson = null;
    try {
        customerInfoJson = objectMapper.writeValueAsString(customerInfo);
    } catch (final JsonProcessingException e) {
        logger.error("JSON writing error", e);
    }

    return customerInfoJson;
    }

    @Override
    public List<Map<String, String>> convertToEntityAttribute(String customerInfoJSON) {

        List<Map<String, String>> customerInfo = null;
        try {
            customerInfo = objectMapper.readValue(customerInfoJSON, List.class);
        } catch (final IOException e) {
            logger.error("JSON reading error", e);
        }

        return customerInfo;
    }
}
