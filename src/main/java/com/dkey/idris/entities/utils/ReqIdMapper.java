package com.dkey.idris.entities.utils;

public enum ReqIdMapper {
    CAR_GOOD_USER_TRUST("19");


    private final String reqId;

    private ReqIdMapper(String reqId){
        this.reqId = reqId;
    }

    public String getReqId(){
        return this.reqId;
    }
}
