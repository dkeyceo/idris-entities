package com.dkey.idris.entities.utils;

import lombok.NoArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

@NoArgsConstructor
public class SecurityContextHandler {

    public String getUsernameAuthority(SecurityContextHolder context) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName;
        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    public boolean getPersonDataAuthority(SecurityContextHolder context, String persDataAuth) {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(persDataAuth));
    }

    public String getUserIPAddr(SecurityContextHolder context) {
        Object details
                = SecurityContextHolder.getContext().getAuthentication().getDetails();
        String ipAddress;
        if (details instanceof WebAuthenticationDetails) {
            ipAddress = ((WebAuthenticationDetails) details).getRemoteAddress();
        } else {
            ipAddress = details.toString();
        }
        return ipAddress;
    }
}
