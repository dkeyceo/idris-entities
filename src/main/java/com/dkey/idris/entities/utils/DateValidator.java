package com.dkey.idris.entities.utils;

public interface DateValidator {
     boolean isValid(String dateStr);
}
