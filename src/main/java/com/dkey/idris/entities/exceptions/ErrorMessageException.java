package com.dkey.idris.entities.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
@Data
public class ErrorMessageException extends RuntimeException {

    private Object obj;
    
    public ErrorMessageException() {

    }

    public ErrorMessageException(String message) {
        super(message);

    }

    public ErrorMessageException(String message, Object obj) {
        super(message);
        this.obj = obj;
    }

}
