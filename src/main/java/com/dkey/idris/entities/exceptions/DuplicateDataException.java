package com.dkey.idris.entities.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class DuplicateDataException extends RuntimeException {

    private Object obj;

    public DuplicateDataException() {

    }

    public DuplicateDataException(String message) {
        super(message);

    }

    public DuplicateDataException(String message, Object obj) {
        super(message);
        this.obj = obj;
    }

}
