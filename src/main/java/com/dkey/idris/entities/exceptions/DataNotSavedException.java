package com.dkey.idris.entities.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Data
@EqualsAndHashCode
public class DataNotSavedException extends RuntimeException {

    private Object obj;

    public DataNotSavedException() {

    }

    public DataNotSavedException(String message) {
        super(message);

    }

    public DataNotSavedException(String message, Object obj) {
        super(message);
        this.obj = obj;
    }

}
