package com.dkey.idris.entities.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
@Data
@EqualsAndHashCode
public class DataNotFoundException extends RuntimeException {

    private Object obj;

    public DataNotFoundException() {

    }

    public DataNotFoundException(String message) {
        super(message);

    }

    public DataNotFoundException(String message, Object obj) {
        super(message);
        this.obj = obj;
    }

}
