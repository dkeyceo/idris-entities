package com.dkey.idris.entities.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN )
public class SignNotRecognizedException extends RuntimeException {

	public SignNotRecognizedException() {

	}

	public SignNotRecognizedException(String message) {
		super(message);

	}

}
