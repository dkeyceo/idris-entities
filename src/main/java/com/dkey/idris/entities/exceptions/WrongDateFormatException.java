package com.dkey.idris.entities.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST )
public class WrongDateFormatException extends RuntimeException {

	public WrongDateFormatException() {

	}

	public WrongDateFormatException(String message) {
		super(message);

	}

}
