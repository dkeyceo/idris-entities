package com.dkey.idris.entities.dict.clnt;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ps_dai$clnt_amplua")
public class ClientAmpluaTypeDict extends BaseDict {
}
