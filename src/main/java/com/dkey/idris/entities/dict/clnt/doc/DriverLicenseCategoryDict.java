package com.dkey.idris.entities.dict.clnt.doc;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$drv_cat")
@Data
public class DriverLicenseCategoryDict extends BaseDict {
    private String code;
}
