package com.dkey.idris.entities.dict.car;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ps_dai$car_reg_status")
@Data
public class CarRegStatusDict extends BaseDict {
    private String code;
}
