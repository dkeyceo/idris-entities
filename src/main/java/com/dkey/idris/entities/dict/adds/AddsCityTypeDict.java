package com.dkey.idris.entities.dict.adds;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$adds_city_type")
public class AddsCityTypeDict extends BaseDict {
}
