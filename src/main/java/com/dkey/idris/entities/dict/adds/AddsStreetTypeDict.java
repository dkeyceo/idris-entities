package com.dkey.idris.entities.dict.adds;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ps_dai$adds_street_type")
public class AddsStreetTypeDict extends BaseDict {

}
