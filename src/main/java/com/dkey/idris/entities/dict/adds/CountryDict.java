package com.dkey.idris.entities.dict.adds;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$country")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryDict {
    @Id
    @Column(name="id")
    private Long id;
    @Column(name="code")
    private String code;
    @Column(name="value")
    private String value;
    private String isUsed;
}