package com.dkey.idris.entities.dict.clnt.doc;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$clnt_doc_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientDocTypeDict extends BaseDict {
    private String code;
}