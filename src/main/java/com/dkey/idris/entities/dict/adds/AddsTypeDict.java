package com.dkey.idris.entities.dict.adds;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$adds_type")
public class AddsTypeDict extends BaseDict {
}
