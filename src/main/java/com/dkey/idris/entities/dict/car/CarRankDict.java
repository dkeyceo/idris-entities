package com.dkey.idris.entities.dict.car;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$car_rank")
@Data
public class CarRankDict extends BaseDict {

    private String code;
}