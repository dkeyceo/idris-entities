package com.dkey.idris.entities.dict.clnt.contact;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ps_dai$clnt_contact_type")
public class ClientContactTypeDict extends BaseDict {

}
