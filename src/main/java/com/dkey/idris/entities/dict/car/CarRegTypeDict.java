package com.dkey.idris.entities.dict.car;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$car_reg_type")
@Data
public class CarRegTypeDict extends BaseDict {
    private String code;
}
