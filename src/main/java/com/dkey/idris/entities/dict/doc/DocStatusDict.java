package com.dkey.idris.entities.dict.doc;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$doc_status")
public class DocStatusDict extends BaseDict {
}