package com.dkey.idris.entities.dict.doc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dkey.idris.entities.dict.BaseDict;

@Entity
@Table(name = "ps_dai$eur_doc_status")
@Data
public class EurDocStatus extends BaseDict {
	
}
