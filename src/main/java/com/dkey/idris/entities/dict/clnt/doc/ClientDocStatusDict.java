package com.dkey.idris.entities.dict.clnt.doc;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PS_DAI$CLNT_DOC_STATUS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientDocStatusDict extends BaseDict {
    private String code;
}