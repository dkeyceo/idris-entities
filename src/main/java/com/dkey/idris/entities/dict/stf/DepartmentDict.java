package com.dkey.idris.entities.dict.stf;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$stf_dep")
@Data
public class DepartmentDict extends BaseDict {
    private String code;
}