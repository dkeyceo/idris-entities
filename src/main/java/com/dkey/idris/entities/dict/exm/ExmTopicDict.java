package com.dkey.idris.entities.dict.exm;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$exm_topic")
@Data
public class ExmTopicDict extends BaseDict {
}
