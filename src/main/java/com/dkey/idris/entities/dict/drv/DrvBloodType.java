package com.dkey.idris.entities.dict.drv;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="v_dai$drv_blood_types")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrvBloodType {

    @Id
    private Long id;
    private String code;
    private String value;
}
