package com.dkey.idris.entities.dict.adds;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.dkey.idris.entities.dict.BaseDict;

import lombok.Data;

@Entity
@Table(name="ps_dai$koatuu")
@Data
public class Koatuu extends BaseDict {
	private String idMain;
	private String isUsed;
	private String cityType;
}
