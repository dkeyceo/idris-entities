package com.dkey.idris.entities.dict.car;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$car_ecology")
public class CarEcologyDict extends BaseDict {
}
