package com.dkey.idris.entities.dict.dir;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$dir_org")
public class OrganizationTypeDict extends BaseDict {
}