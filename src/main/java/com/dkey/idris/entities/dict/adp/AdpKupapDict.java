package com.dkey.idris.entities.dict.adp;

import com.dkey.idris.entities.dict.BaseDict;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$adp_kupap")
public class AdpKupapDict extends BaseDict {
}
