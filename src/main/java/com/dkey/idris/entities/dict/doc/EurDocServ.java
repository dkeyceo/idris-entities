package com.dkey.idris.entities.dict.doc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dkey.idris.entities.dict.BaseDict;

@Entity
@Table(name = "ps_dai$eur_doc_services")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EurDocServ extends BaseDict {
	
	private String code;
	private String isUsed;
}
