package com.dkey.idris.entities.dict.clnt;

import com.dkey.idris.entities.dict.BaseDict;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$clnt_link_type")
@Data
public class ClientLinkTypeDict extends BaseDict {
    private String code;
}
