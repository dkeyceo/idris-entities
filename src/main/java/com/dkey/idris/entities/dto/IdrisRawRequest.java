package com.dkey.idris.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdrisRawRequest {

    private String payload;
    private String signature;

    public IdrisRawRequest (Map<String, Object> body) {
        if(body.containsKey("payload")) {
            this.payload = body.get("payload").toString();
            this.signature = body.get("signature").toString();
        }
    }
}
