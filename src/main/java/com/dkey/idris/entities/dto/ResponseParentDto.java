package com.dkey.idris.entities.dto;

public interface ResponseParentDto {

    void setResultCodeText(String resultCodeText);

    String getResultCodeText();

    void setResultCode(Integer resultCode);
}
