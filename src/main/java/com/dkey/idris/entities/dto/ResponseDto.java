package com.dkey.idris.entities.dto;

import com.dkey.idris.entities.utils.RandomIdGenerator;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Data
public class ResponseDto extends RepresentationModel<ResponseDto> implements ResponseParentDto {

    private String requestId;
    private LocalDateTime requestDate;
    private String responseId;
    private LocalDateTime responseDate;
    private Integer resultCode;
    private String resultCodeText;
    private String serviceName;
    private List body;


    @JsonCreator
    public ResponseDto() {
        RandomIdGenerator randomIdGenerator = new RandomIdGenerator();
        this.requestId = randomIdGenerator.getRandomId();
        this.requestDate = LocalDateTime.now();
        this.responseId = randomIdGenerator.getRandomId();
        this.responseDate = LocalDateTime.now();
        this.resultCode = 204;
        this.resultCodeText = "NoContent";
        List<String> data = new LinkedList();
        this.body = data;
//                Collections.singletonList(data.add("No content"));
    }

    @JsonCreator
    public ResponseDto(@JsonProperty("body") List body) {
        RandomIdGenerator randomIdGenerator = new RandomIdGenerator();
        this.requestId = randomIdGenerator.getRandomId();
        this.requestDate = LocalDateTime.now();
        this.responseId = randomIdGenerator.getRandomId();
        this.responseDate = LocalDateTime.now();

        this.body = body;
        this.resultCode = 200;
        this.resultCodeText = "OK";
    }


}
