package com.dkey.idris.entities.dto.docs;

import com.dkey.idris.entities.dict.doc.EurDocServ;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EurDocServDto {
	
    private String id;
    private String value;
    private String code;
    private String isUsed;
    
    public EurDocServDto(EurDocServ eurDocServ){
        this.id = eurDocServ.getId();
        this.code = eurDocServ.getCode();
        this.value = eurDocServ.getValue();
        this.isUsed = eurDocServ.getIsUsed();
    }
}
