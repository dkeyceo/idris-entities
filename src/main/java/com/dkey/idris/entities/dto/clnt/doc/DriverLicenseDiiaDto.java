package com.dkey.idris.entities.dto.clnt.doc;

import com.dkey.idris.entities.domain.adds.clnt.ClientAddressDomain;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import com.dkey.idris.entities.domain.clnt.doc.DriverLicenseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class DriverLicenseDiiaDto {

    private List<DriverLicenseDomain> driverLicense;
    private ClientDomain clientDomain;
    private List<ClientAddressDomain> clientAddr;

    public DriverLicenseDiiaDto(List<DriverLicenseDomain> driverLicenseDomain, ClientDomain clientDomain, List<ClientAddressDomain> clientAddr){
        this.driverLicense = driverLicenseDomain.stream().filter(f->f.getDocType().getId()=="200").collect(Collectors.toList());
        this.clientDomain = clientDomain;
        this.clientAddr = clientAddr.stream().filter(c->c.getAddressType().getId()=="103" && c.getIsReal() == "T").collect(Collectors.toList());
    }
}
