package com.dkey.idris.entities.dto.adds;

import java.util.HashMap;
import java.util.Map;

import com.dkey.idris.entities.domain.adds.StreetRegEdocDomain;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StreetRegEdocDTO {
	@JsonProperty("COUNTRY")
	private Map<String , String> country;
	@JsonProperty("REGION")
	private Map<String, String> region;
	@JsonProperty("CITY_TYPE")
	private Map<String , String> cityType;
	@JsonProperty("CITY")
	private Map<String, String> city;
	@JsonProperty("STREET_TYPE")
	private Map<String, String> streetType;
	@JsonProperty("STREET")
	private Map<String, String> street;
	@JsonProperty("N_HOUSE")
	private String nHouse;
	@JsonProperty("S_HOUSE")
	private String sHouse;
	@JsonProperty("CORPS")
	private String corps;
	@JsonProperty("N_FLAT")
	private String nFlat;
	@JsonProperty("S_FLAT")
	private String sFlat;
	@JsonProperty("POSTAL_CODE")
	private String postalCode;
	
	public StreetRegEdocDTO(StreetRegEdocDomain streetRegEdocDomain) {
		
		HashMap country = new HashMap<String, String>();
		country.put("ID", streetRegEdocDomain.getCountryId());
		country.put("VALUE", streetRegEdocDomain.getCountryName());
		this.country = country;
		
		HashMap region = new HashMap<String, String>();
		region.put("ID", streetRegEdocDomain.getRegionId());
		region.put("VALUE", streetRegEdocDomain.getRegionName());
		this.region = region;
		
		HashMap cityType = new HashMap<String, String>();
		cityType.put("ID", streetRegEdocDomain.getCityTypeId());
		cityType.put("VALUE", streetRegEdocDomain.getCityTypeName());
		this.cityType = cityType;
		
		HashMap city = new HashMap<String, String>();
		city.put("ID", streetRegEdocDomain.getCityId());
		city.put("VALUE", streetRegEdocDomain.getCityName());
		this.city = city;
		
		HashMap streetType = new HashMap<String, String>();
		streetType.put("ID", streetRegEdocDomain.getStreetTypeId());
		streetType.put("VALUE", streetRegEdocDomain.getStreetTypeName());
		this.streetType = streetType;
		
		HashMap street = new HashMap<String, String>();
		street.put("ID", streetRegEdocDomain.getStreetId());
		street.put("VALUE", streetRegEdocDomain.getStreetName());
		this.street = street;
		
		this.nHouse = streetRegEdocDomain.getNHouse();
		this.sHouse = streetRegEdocDomain.getSHouse();
		
		this.corps = streetRegEdocDomain.getCorps();
		
		this.nFlat = streetRegEdocDomain.getNFlat();
		
		this.sFlat = streetRegEdocDomain.getSFlat();
		
		this.postalCode = streetRegEdocDomain.getPostalCode();
		
	}
}
