package com.dkey.idris.entities.dto.car;

import com.dkey.idris.entities.domain.car.CarDocNregDomain;
import com.dkey.idris.entities.domain.car.CarDocPaperDomain;
import com.dkey.idris.entities.domain.car.CarDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarSprDiiaDto {
    private Long paperId;
    private String licensePlate;
    private Long makeYear;
    private String brand;
    private String model;
    private String kindBody;
    private String vin;
    private Long totalWeight;
    private Long ownWeight;
    private String rankCategory;
    private Long capacity;
    private String fuel;
    private String color;
    private LocalDate dreg;
    private Long nseating;
    private Long nstandup;
    private LocalDate dfirstReg;

    public CarSprDiiaDto(CarDocPaperDomain carDocPaperDomain) {
        this.paperId = Long.parseLong(carDocPaperDomain.getPaperId());
        this.licensePlate = carDocPaperDomain.getCarDocs().getPlates()
                .stream()
                .filter(plate -> plate.getDocId().equals(carDocPaperDomain.getDocId()))
                .map(CarDocNregDomain::getLicensePlate)
                .collect(Collectors.joining())
        ;
        this.dreg = carDocPaperDomain.getClientDoc().getDDoc();
        CarDomain carDomain = carDocPaperDomain.getCarDocs().getCar();
        this.makeYear = carDomain.getMakeYear();
        this.brand = carDomain.getCarBrand().getValue();
        this.model = carDomain.getCarModel().getValue();
        this.kindBody = carDomain.getCarKind().getValue();
        this.vin = carDomain.getVin();
        this.totalWeight = carDomain.getTotalWeight();
        this.ownWeight = carDomain.getOwnWeight();
        this.rankCategory = carDomain.getCarRank().getCode();
        this.capacity = carDomain.getCapacity();
        this.fuel = carDomain.getCarFuel().getValue();
        this.color = carDomain.getCarColor().getValue();
        this.nseating = carDomain.getNSeating();
        this.nstandup = carDomain.getNStandup();
        this.dfirstReg = carDomain.getDFirstRegistration();
    }
}
