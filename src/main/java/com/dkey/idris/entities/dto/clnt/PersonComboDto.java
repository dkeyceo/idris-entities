package com.dkey.idris.entities.dto.clnt;

import com.dkey.idris.entities.dict.adds.CountryDict;
import com.dkey.idris.entities.dict.clnt.ClientGenderTypeDict;
import com.dkey.idris.entities.dict.clnt.ClientPersonTypeDict;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonComboDto {

    private String clientId;
    private Map country;
    private String lastNameU;
    private String firstNameU;
    private String middleNameU;
    private String lastNameL;
    private String firstNameL;
    private String middleNameL;
    private LocalDate birthDay;
    @JsonProperty("gender")
    private ClientGenderTypeDict clientGenderTypeDict;
    @JsonProperty("person")
    private ClientPersonTypeDict clientPersonTypeDict;
    @JsonProperty("innChar")
    private String inn;
    private String noInn;

    public PersonComboDto(Person person) {
        this.clientId = person.getClientId();
        this.lastNameU = person.getLastNameU();
        this.firstNameU = person.getFirstNameU();
        this.middleNameU = person.getMiddleNameU();
        this.lastNameL = person.getLastNameL();
        this.firstNameL = person.getFirstNameL();
        this.middleNameL = person.getMiddleNameL();
        this.birthDay = person.getBirthDay();
        this.clientGenderTypeDict = person.getClientGenderTypeDict();
        this.clientPersonTypeDict = person.getClientPersonTypeDict();
        this.inn = person.getInn();
        this.noInn = person.getNoInn();

        Map country = new HashMap();
        country.put("ID", String.format("%d", person.getCitizenship().getId()));
        country.put("VALUE", person.getCitizenship().getValue());
        this.country = country;
    }
}
