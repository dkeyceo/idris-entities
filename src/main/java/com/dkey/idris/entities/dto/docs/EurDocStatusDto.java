package com.dkey.idris.entities.dto.docs;

import com.dkey.idris.entities.dict.doc.EurDocStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EurDocStatusDto {

    private String id;
    private String value;

    public EurDocStatusDto(EurDocStatus eurDocStatus){
        this.id = eurDocStatus.getId();
        this.value = eurDocStatus.getValue();
    }
}
