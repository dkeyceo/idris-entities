package com.dkey.idris.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdrisRequest {

    private String requestId;
    private LocalDate requestDateTime;
    private IdrisResource resource;
    private String body;
    private IdrisRequester requester;
    private String baseRequest;
    private String requestMeta;
}
