package com.dkey.idris.entities.dto.clnt.doc;

import com.dkey.idris.entities.domain.clnt.doc.ClientDocDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientSprDiiaDto {
    private Long id;
    private Long clientId;
    private Map department;
    private Map docType;
    private Map status;
    private Map country;
    private String ndoc;
    private LocalDate ddoc;
    private String sdoc;
    private LocalDate dend;

    public ClientSprDiiaDto(ClientDocDomain clientDocDomain){
        this.id = Long.parseLong(clientDocDomain.getPaperId());
        this.clientId = Long.parseLong(clientDocDomain.getClntId());

        Map department = new HashMap();
        department.put("ID", clientDocDomain.getDepartmentDict().getId());
        department.put("VALUE", clientDocDomain.getDepartmentDict().getValue());
        this.department = department;

        Map docType = new HashMap();
        docType.put("ID", clientDocDomain.getDocType().getId());
        docType.put("VALUE", clientDocDomain.getDocType().getValue());
        this.docType = docType;

        Map status = new HashMap();
        status.put("ID", clientDocDomain.getDocStatus().getId());
        status.put("VALUE", clientDocDomain.getDocStatus().getValue());
        this.status = status;

        Map country = new HashMap();
        country.put("ID", clientDocDomain.getCountryDict().getId());
        country.put("VALUE", clientDocDomain.getCountryDict().getValue());
        this.country = country;

        this.ndoc = clientDocDomain.getNDoc();
        this.ddoc = clientDocDomain.getDDoc();
        this.sdoc = clientDocDomain.getSDoc();
        this.dend = clientDocDomain.getDEnd();

    }
}
