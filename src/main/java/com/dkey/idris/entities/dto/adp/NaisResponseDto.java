package com.dkey.idris.entities.dto.adp;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NaisResponseDto {

    private String naisResponseId;
    private String naisGenTime;

}
