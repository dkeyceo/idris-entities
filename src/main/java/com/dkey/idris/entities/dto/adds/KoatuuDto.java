package com.dkey.idris.entities.dto.adds;

import com.dkey.idris.entities.dict.adds.Koatuu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KoatuuDto {
	
	private String id;
	private String value;
	private String idMain;
	private String isUsed;
	private String cityType;
	
	
	public KoatuuDto(Koatuu koatuu) {
		
		this.id = koatuu.getId();
		this.value = koatuu.getValue();
		this.cityType = koatuu.getCityType();
		this.idMain = koatuu.getIdMain();
		this.isUsed = koatuu.getIsUsed();
	}
	
	
}
