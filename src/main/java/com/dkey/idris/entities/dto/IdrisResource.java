package com.dkey.idris.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdrisResource {
    private String resourceId;
    private String name;
    private String env;
    private String method;
    private String URI;

    private Map<String, Object> params;
    private Map<String, Object> body;
    private Map<String, Object> headers;

    private IdrisResource idrisResourceAuth;

    public IdrisResource (String resourceId, String name) {
        this.resourceId = resourceId;
        this.name = name;
    }

    public IdrisResource(String resourceId, String name, String env, String method, String uRI,
                         Map<String, Object> params) {
        super();
        this.resourceId = resourceId;
        this.name = name;
        this.env = env;
        this.method = method;
        URI = uRI;
        this.params = params;
    }


}
