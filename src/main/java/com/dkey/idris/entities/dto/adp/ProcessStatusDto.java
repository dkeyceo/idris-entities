package com.dkey.idris.entities.dto.adp;

import com.dkey.idris.entities.domain.adp.AdpDocs;
import com.google.gson.Gson;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class ProcessStatusDto {

    private Map processStatus;

    public ProcessStatusDto(AdpDocs adpDocs) {
        this.processStatus = getJsonProcessStatus(adpDocs.getProcessStatus());
    }

    private Map getJsonProcessStatus(String processStatus) {
        Gson gSon = new Gson();
        Map<String, Object> processStatusMap
                = gSon.fromJson(String.valueOf(processStatus), Map.class);
        return processStatusMap;
    }
}
