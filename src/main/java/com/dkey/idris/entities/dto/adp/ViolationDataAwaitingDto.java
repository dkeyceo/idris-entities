package com.dkey.idris.entities.dto.adp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "adpDocAwaiting")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViolationDataAwaitingDto {

    @Id
    private ObjectId id;
    @Indexed
    private String queueId;
    private String jsonData;
    private String rawBody;

    private String naisDocId;
    private Boolean isNaisDocId;

    private Boolean isRefferalToNais;
    private NaisResponseDto naisResponseDto;
    private Boolean isNaisResponse;

    private Map requestType;
    private String sfapRequestId;
    private String serialDoc;
    private String numberDoc;
    private String dateDoc;

    private Boolean isSent;
    private String sentTo;

    private Map adpStatus;

    private String requestTime;

}
