package com.dkey.idris.entities.dto.adds;

import java.util.HashMap;
import java.util.Map;

import com.dkey.idris.entities.domain.adds.AddressRegEdocDomain;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressRegEdocDTO {
	@JsonProperty("COUNTRY")
	private Map<String , String> country;
	@JsonProperty("REGION")
	private Map<String, String> region;
	@JsonProperty("CITY_TYPE")
	private Map<String , String> cityType;
	@JsonProperty("CITY")
	private Map<String, String> city;
	@JsonProperty("STREET_TYPE")
	private Map<String, String> streetType;
	@JsonProperty("STREET")
	private Map<String, String> street;
	@JsonProperty("N_HOUSE")
	private String nHouse;
	@JsonProperty("S_HOUSE")
	private String sHouse;
	@JsonProperty("CORPS")
	private String corps;
	@JsonProperty("N_FLAT")
	private String nFlat;
	@JsonProperty("S_FLAT")
	private String sFlat;
	@JsonProperty("POSTAL_CODE")
	private String postalCode;
	
	public AddressRegEdocDTO(AddressRegEdocDomain addressRegEdocDomain) {
		
		HashMap country = new HashMap<String, String>();
		country.put("ID", addressRegEdocDomain.getCountryId());
		country.put("VALUE", addressRegEdocDomain.getCountryName());
		this.country = country;
		
		HashMap region = new HashMap<String, String>();
		region.put("ID", addressRegEdocDomain.getRegionId());
		region.put("VALUE", addressRegEdocDomain.getRegionName());
		this.region = region;
		
		HashMap cityType = new HashMap<String, String>();
		cityType.put("ID", addressRegEdocDomain.getCityTypeId());
		cityType.put("VALUE", addressRegEdocDomain.getCityTypeName());
		this.cityType = cityType;
		
		HashMap city = new HashMap<String, String>();
		city.put("ID", addressRegEdocDomain.getCityId());
		city.put("VALUE", addressRegEdocDomain.getCityName());
		this.city = city;
		
		HashMap streetType = new HashMap<String, String>();
		streetType.put("ID", addressRegEdocDomain.getStreetTypeId());
		streetType.put("VALUE", addressRegEdocDomain.getStreetTypeName());
		this.streetType = streetType;
		
		HashMap street = new HashMap<String, String>();
		street.put("ID", addressRegEdocDomain.getStreetId());
		street.put("VALUE", addressRegEdocDomain.getStreetName());
		this.street = street;
		
		this.nHouse = addressRegEdocDomain.getNHouse();
		this.sHouse = addressRegEdocDomain.getSHouse();
		
		this.corps = addressRegEdocDomain.getCorps();
		
		this.nFlat = addressRegEdocDomain.getNFlat();
		
		this.sFlat = addressRegEdocDomain.getSFlat();
		
		this.postalCode = addressRegEdocDomain.getPostalCode();
		
	}
}
