package com.dkey.idris.entities.dto.drv;

import com.dkey.idris.entities.domain.drv.DrvLicsClntIdOnly;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DrvLicsClntIdOnlyDto {

    private Long clientId;

    public DrvLicsClntIdOnlyDto(DrvLicsClntIdOnly drvLicsClntIdOnly){

        this.clientId = drvLicsClntIdOnly.getClientId();

    }
}
