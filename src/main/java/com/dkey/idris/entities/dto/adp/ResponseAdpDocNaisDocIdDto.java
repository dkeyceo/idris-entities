package com.dkey.idris.entities.dto.adp;


import com.dkey.idris.entities.domain.adp.AdpReqToDoc;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseAdpDocNaisDocIdDto {

    private Long requestId;
    private Long docId;
    private String seria;
    private Long number;
    
    public ResponseAdpDocNaisDocIdDto(AdpReqToDoc adpReqToDoc){
        this.requestId = adpReqToDoc.getRequestId();
        this.docId = adpReqToDoc.getDocId();
        this.seria = adpReqToDoc.getSeria();
        this.number = adpReqToDoc.getNumber();
    }
    
}
