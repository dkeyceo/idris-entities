package com.dkey.idris.entities.dto.drv;

import com.dkey.idris.entities.domain.drv.DriverLicence;
import com.dkey.idris.entities.domain.drv.DriverLicenceSp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DriverLicenceOpenData {

    private Long paperId;
    private List<Map<String, String>> categories;
    private String sDoc;
    private String nDoc;
    private LocalDate dDoc;
    private LocalDate dEnd;
    private String depUA;
    private String depUS;
    private Map status;

    public DriverLicenceOpenData(DriverLicence driverLicence) {
        this.paperId = driverLicence.getPaperId();
        this.categories = driverLicence.getCategories();
        this.sDoc = driverLicence.getSeria();
        this.nDoc = driverLicence.getNumber();
        this.dDoc = driverLicence.getDateIssued();
        this.dEnd = driverLicence.getDateEnd();
        this.depUA = driverLicence.getDepUA();
        this.depUS = driverLicence.getDepUS();

        Map status = new HashMap();
        status.put("statusId", driverLicence.getStatusId());
        status.put("status", driverLicence.getStatus());
        this.status = status;

    }

    public DriverLicenceOpenData(DriverLicenceSp driverLicence) {
        this.paperId = driverLicence.getPaperId();
        this.categories = driverLicence.getCategories();
        this.sDoc = driverLicence.getSeria();
        this.nDoc = driverLicence.getNumber();
        this.dDoc = driverLicence.getDateIssued();
        this.dEnd = driverLicence.getDateEnd();
        this.depUA = driverLicence.getDepUA();
        this.depUS = driverLicence.getDepUS();

        Map status = new HashMap();
        status.put("statusId", driverLicence.getStatusId());
        status.put("status", driverLicence.getStatus());
        this.status = status;

    }
}
