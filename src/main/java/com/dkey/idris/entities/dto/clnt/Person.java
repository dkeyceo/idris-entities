package com.dkey.idris.entities.dto.clnt;

import com.dkey.idris.entities.dict.adds.CountryDict;
import com.dkey.idris.entities.dict.clnt.ClientPersonTypeDict;
import com.dkey.idris.entities.dict.clnt.ClientGenderTypeDict;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person extends ClientDomain {

    private String clientId;
    private CountryDict citizenship;
    private String lastNameU;
    private String firstNameU;
    private String middleNameU;
    private String lastNameL;
    private String firstNameL;
    private String middleNameL;
    private LocalDate birthDay;
    private ClientGenderTypeDict clientGenderTypeDict;
    private ClientPersonTypeDict clientPersonTypeDict;
    private String inn;

    private String noInn;

    public Person() {
    }

    public Person(ClientDomain clientDomain) {
        this.clientId = clientDomain.getClientId();
        this.lastNameU = clientDomain.getName1U();
        this.firstNameU = clientDomain.getName2U();
        this.middleNameU = clientDomain.getName3U();
        this.lastNameL = clientDomain.getName1L();
        this.firstNameL = clientDomain.getName2L();
        this.middleNameL = clientDomain.getName3L();
        this.birthDay = clientDomain.getBirthDay();
        this.clientGenderTypeDict = clientDomain.getGenderType();
        this.clientPersonTypeDict = clientDomain.getPersonType();
        this.inn = clientDomain.getInn();
        this.noInn = clientDomain.getNoInn();
        this.citizenship = clientDomain.getCountry();
    }
}
