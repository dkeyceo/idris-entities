package com.dkey.idris.entities.dto;

import com.dkey.idris.entities.domain.adds.clnt.ClientAddressDomain;
import com.dkey.idris.entities.dto.clnt.PersonComboDto;
import com.dkey.idris.entities.dto.clnt.doc.SprCertificateDiiaDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


@Data
@NoArgsConstructor
public class ComboDiiaSprDto implements Serializable {
    private List<SprCertificateDiiaDto> regSertificate;
    private PersonComboDto client;
    private List<ClientAddressDomain> clientAddr;

}
