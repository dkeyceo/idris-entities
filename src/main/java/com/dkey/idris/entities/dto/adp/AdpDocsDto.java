package com.dkey.idris.entities.dto.adp;

import com.dkey.idris.entities.domain.adp.AdpDocs;
import com.google.gson.Gson;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class AdpDocsDto {

    private String docId;
    private LocalDate dSystem;
    private LocalDate lastUpdateDate;
    private Map department;

    private String fabula;
    private Map kupap;
    private LocalDateTime dFail;

    private Map adpAddr;
    private Map send;
    private LocalDate dDirect;
    private Map depCreated;
    private Map decision;
    private Map penalty;
    private LocalDate dCancel;
    private String licensePlate;
    private String brandName;
    private Map pdd;
    private String lastNameU;
    private String firstNameU;
    private String middleNameU;
    private LocalDate birthday;
    private String lastNameR;
    private String firstNameR;
    private String middleNameR;

    private Map document;

    private Map status;
    private Map mark;
    private String paidInfo;
    private Long clntId;

    private Map direction;

    private Long makeTypeId;
    private Integer isAuto;
    private Map processStatus;
    private String region;

    public AdpDocsDto(AdpDocs adpDocs) {
        this.docId = adpDocs.getDocId();
        this.dSystem = adpDocs.getDSystem();
        this.lastUpdateDate = adpDocs.getLastUpdateDate();

        Map department = new HashMap();
        department.put("ID", adpDocs.getDepId());
        department.put("NAME", adpDocs.getDep());
        this.department = department;

        this.fabula = adpDocs.getFabula();

        Map kupap = new HashMap();
        kupap.put("ID", adpDocs.getKupapId());
        kupap.put("NAME", adpDocs.getKupap());
        this.kupap = kupap;

        this.dFail = adpDocs.getDFail();

        Map adpAddr = new HashMap();
        adpAddr.put("OB", adpDocs.getOb());
        adpAddr.put("RN", adpDocs.getRn());
        adpAddr.put("NS", adpDocs.getNs());
        adpAddr.put("UL", adpDocs.getUl());
        adpAddr.put("ROADKM", adpDocs.getRoadKm());
        adpAddr.put("address", adpDocs.getAddress());
        this.adpAddr = adpAddr;

        Map send = new HashMap();
        send.put("ID", adpDocs.getSendId());
        send.put("NAME", adpDocs.getSend());
        this.send = send;

        this.dDirect = adpDocs.getDDirect();

        Map depCreated = new HashMap();
        depCreated.put("ID", adpDocs.getDepCreatedId());
        depCreated.put("NAME", adpDocs.getDepCreated());
        this.depCreated = depCreated;

        Map decision = new HashMap();
        decision.put("ID", adpDocs.getDecisionId());
        decision.put("NAME", adpDocs.getDecision());
        this.decision = decision;

        Map penalty = new HashMap();
        penalty.put("PENALDATE", adpDocs.getDPenalty());
        penalty.put("PENALID", adpDocs.getPenaltyId());
        penalty.put("PENALNAME", adpDocs.getPenalty());
        penalty.put("NTERM", adpDocs.getNTerm());
        penalty.put("PAYTERM", adpDocs.getPayTerm());
        penalty.put("DPAYTERM", adpDocs.getDPayTerm());
        penalty.put("PENALADDID", adpDocs.getPenaltyAddId());
        penalty.put("PENALTYADD", adpDocs.getPenasltyAdd());
        this.penalty = penalty;

        this.dCancel = adpDocs.getDCancel();
        this.licensePlate = adpDocs.getLicensePlate();
        this.brandName = adpDocs.getBrandName();

        Map pdd = new HashMap();
        pdd.put("ID", adpDocs.getPddId());
        pdd.put("NAME", adpDocs.getPdd());
        this.pdd = pdd;

        this.lastNameU = adpDocs.getLastNameU();
        this.firstNameU = adpDocs.getFirstNameU();
        this.middleNameU = adpDocs.getMiddleNameU();
        this.birthday = adpDocs.getBirthday();
        this.lastNameR = adpDocs.getLastNameR();
        this.firstNameR = adpDocs.getFirstNameR();
        this.middleNameR = adpDocs.getMiddleNameR();

        Map document = new HashMap();
        document.put("SERIA", adpDocs.getSeria());
        document.put("NUMBER", adpDocs.getNumber());
        document.put("DOCDATE", adpDocs.getDateReg());
        this.document = document;

        Map status = new HashMap();
        status.put("ID", adpDocs.getStatusId());
        status.put("NAME", adpDocs.getStatus());
        this.status = status;

        Map mark = new HashMap();
        mark.put("ID", adpDocs.getMarkId());
        mark.put("NAME", adpDocs.getMarkName());
        this.mark = mark;

        this.paidInfo = adpDocs.getPaidInfo();
        this.clntId = adpDocs.getClntId();

        Map direction = new HashMap();
        direction.put("directionId", adpDocs.getDirectionId());
        direction.put("direction", adpDocs.getDirection());
        this.direction = direction;

        this.makeTypeId = adpDocs.getMakeTypeId();
        this.isAuto = (this.makeTypeId == 1024) ? 1 : 0;
        this.processStatus = getJsonProcessStatus(adpDocs.getProcessStatus());
        this.region = adpDocs.getRegionId();

    }
    private Map getJsonProcessStatus(String processStatus) {
        Gson gSon = new Gson();
        Map<String, Object> processStatusMap = 
                gSon.fromJson(String.valueOf(processStatus), Map.class);
        return processStatusMap;
    }

}
