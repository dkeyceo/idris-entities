package com.dkey.idris.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdrisRequester {

    private String fullName;
    private String subjOrg;
    private String subjOrgUnit;
    private String subjTitle;
    private String subjDrfoCode;
    private String subjEmail;
    private String subjEdrpouCode;
    private String subjPhone;
    private String subjAddress;
    private String dsign;

}
