package com.dkey.idris.entities.dto.adp;

import com.dkey.idris.entities.domain.adp.AdpsCard;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpsCardDto {
    
    @JsonProperty("NAIS_DOC")
    private Map naisDoc;
    @JsonProperty("OWNER")
    private Map owner;
    @JsonProperty("DRIVE_LICENSE")
    private Map driveLicense;
    @JsonProperty("SPR_LICENSE")
    private Map sprLicense;
    @JsonProperty("ADDRESS")
    private Map address;
    @JsonProperty("VEHICLE")
    private Map vehicle;
    @JsonProperty("TEXTREG")
    private String textReg;
    @JsonProperty("PROPER_USER")
    private Map properUser;
//    private List properUser;
  
    public AdpsCardDto(AdpsCard adpsCard) {

        HashMap naisDoc = new HashMap();
        naisDoc.put("DDDOC_ID",adpsCard.getDddDocId());
        naisDoc.put("DCDCAR_ID",adpsCard.getDcdCarId());
        naisDoc.put("DCDBRAND_ID", adpsCard.getDcdBrandId());
        naisDoc.put("DCDMODEL_ID",adpsCard.getDcdModelId());
        naisDoc.put("DCDKIND_ID", adpsCard.getDcdKindId());
        naisDoc.put("DCDRANK_ID", adpsCard.getDcdRankId());
        naisDoc.put("DCDBODY_ID", adpsCard.getDcdBodyId());
        naisDoc.put("DCDCOLOR_ID", adpsCard.getDcdColorId());
        naisDoc.put("DCDCLNT_ID", adpsCard.getDcdClntId());
        naisDoc.put("DCDCLNTVER_ID", adpsCard.getDcdClntVerId());
        naisDoc.put("PAPER_ID", adpsCard.getPaperId());
        naisDoc.put("DRV_ID", adpsCard.getDrvId());
        naisDoc.put("R_ADD_ID", adpsCard.getRAddId());
        this.naisDoc = naisDoc;
        HashMap owner = new HashMap();
        owner.put("TYPE",adpsCard.getType());
        owner.put("LAST_NAME", adpsCard.getLastName());
        owner.put("FIRST_NAME", adpsCard.getFirstName());
        owner.put("MIDDLE_NAME", adpsCard.getMiddleName());
        owner.put("DATE_OF_BIRTH", adpsCard.getDateOfBirth());
        owner.put("IPN", adpsCard.getIpn());
        owner.put("NATIONALITY", adpsCard.getNationality());
        owner.put("BIRTH_ADDRESS", adpsCard.getBirthAddress());
        owner.put("SEX", adpsCard.getSex());
        HashMap ownerCertificate = new HashMap();
        ownerCertificate.put("TYPE", adpsCard.getPassportType());
        ownerCertificate.put("SERIES", adpsCard.getPassportSeries());
        ownerCertificate.put("NUM", adpsCard.getPassportNum());
        ownerCertificate.put("AUTHORITY", adpsCard.getPassportAuthority());
        ownerCertificate.put("DATE", adpsCard.getPassportDate());
        owner.put("CERTIFICATE",ownerCertificate);
        this.owner = owner;
        HashMap driveLicense = new HashMap();
        driveLicense.put("STATE", adpsCard.getLicType());
        driveLicense.put("SERIES", adpsCard.getLicSeries());
        driveLicense.put("NUM", adpsCard.getLicNum());
        driveLicense.put("AUTHORITY", adpsCard.getLicAuthority());
        driveLicense.put("DATE", adpsCard.getLicDate());
        driveLicense.put("DATE_OF_CANCELLATION", adpsCard.getLicDateOfCancelattion());
        driveLicense.put("CATEGORIES", adpsCard.getCategories());
        this.driveLicense = driveLicense;
        HashMap sprLicense = new HashMap();
        sprLicense.put("STATE", adpsCard.getSprType());
        sprLicense.put("SERIES", adpsCard.getSprSeries());
        sprLicense.put("NUM", adpsCard.getSprNum());
        sprLicense.put("AUTHORITY", adpsCard.getSprAuthority());
        sprLicense.put("DATE", adpsCard.getSprDate());
        sprLicense.put("DATE_OF_CANCELLATION", adpsCard.getSprDateOfCancelattion());
        this.sprLicense = sprLicense;
        HashMap address = new HashMap();
        address.put("R_ADDRESS_NAME",adpsCard.getRAddressName());
        address.put("COUNTRY", adpsCard.getCountry());
        address.put("KOATUU#R", adpsCard.getKoatuu());
        address.put("CITY", adpsCard.getCity());
        address.put("TCITY_FULL", adpsCard.getTCityFull());
        address.put("R_TSTREET_FULL", adpsCard.getRTstreetFull());
        address.put("STREET", adpsCard.getStreet());
        address.put("N_HOUSE", adpsCard.getNHouse());
        address.put("APT", adpsCard.getApt());
        this.address = address;
        HashMap vehicle = new HashMap();
        vehicle.put("LICENSEPLATE", adpsCard.getLicensePlate());
        vehicle.put("VIN", adpsCard.getVin());
        vehicle.put("BRAND", adpsCard.getBrand());
        vehicle.put("MODEL", adpsCard.getModel());
        vehicle.put("TYPE", adpsCard.getCarType());
        vehicle.put("MAKE_YEAR", adpsCard.getMakeYear());
        vehicle.put("DATE_FIRST_REGISTRATION", adpsCard.getDateFirstRegistration());
        vehicle.put("DATE_REGISTRATION", adpsCard.getDateRegistration());
        HashMap tsc = new HashMap();
        tsc.put("TSC_NAME", adpsCard.getTscName());
        tsc.put("NUMBER", adpsCard.getTscNumber());
        vehicle.put("TSC", tsc);
        this.vehicle = vehicle;
        
//        HashMap properUser = new HashMap();
//        this.properUser = properUser;
        
        this.textReg = "ЄДРТЗ";

    }

}
