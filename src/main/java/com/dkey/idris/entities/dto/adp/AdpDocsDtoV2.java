package com.dkey.idris.entities.dto.adp;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;

import com.dkey.idris.entities.domain.adp.AdpDocs;
import com.dkey.idris.entities.domain.adp.AdpDocsV2;
import com.google.gson.Gson;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class AdpDocsDtoV2 extends AdpDocsDto{
	private String penaltySum;
	private String carId;
	
	public AdpDocsDtoV2(AdpDocsV2 adpDocs) {
		setDocId(adpDocs.getDocId());
        setDSystem(adpDocs.getDSystem());
        setLastUpdateDate(adpDocs.getLastUpdateDate());

        Map department = new HashMap();
        department.put("ID", adpDocs.getDepId());
        department.put("NAME", adpDocs.getDep());
        setDepartment( department);

        setFabula(adpDocs.getFabula());

        Map kupap = new HashMap();
        kupap.put("ID", adpDocs.getKupapId());
        kupap.put("NAME", adpDocs.getKupap());
        setKupap(kupap) ;

        setDFail(adpDocs.getDFail());

        Map adpAddr = new HashMap();
        adpAddr.put("OB", adpDocs.getOb());
        adpAddr.put("RN", adpDocs.getRn());
        adpAddr.put("NS", adpDocs.getNs());
        adpAddr.put("UL", adpDocs.getUl());
        adpAddr.put("ROADKM", adpDocs.getRoadKm());
        adpAddr.put("address", adpDocs.getAddress());
        setAdpAddr(adpAddr);

        Map send = new HashMap();
        send.put("ID", adpDocs.getSendId());
        send.put("NAME", adpDocs.getSend());
        setSend(send);

        setDDirect(adpDocs.getDDirect());

        Map depCreated = new HashMap();
        depCreated.put("ID", adpDocs.getDepCreatedId());
        depCreated.put("NAME", adpDocs.getDepCreated());
        setDepCreated(depCreated);

        Map decision = new HashMap();
        decision.put("ID", adpDocs.getDecisionId());
        decision.put("NAME", adpDocs.getDecision());
        setDecision(decision);

        Map penalty = new HashMap();
        penalty.put("PENALDATE", adpDocs.getDPenalty());
        penalty.put("PENALID", adpDocs.getPenaltyId());
        penalty.put("PENALNAME", adpDocs.getPenalty());
        penalty.put("NTERM", adpDocs.getNTerm());
        penalty.put("PAYTERM", adpDocs.getPayTerm());
        penalty.put("DPAYTERM", adpDocs.getDPayTerm());
        penalty.put("PENALADDID", adpDocs.getPenaltyAddId());
        penalty.put("PENALTYADD", adpDocs.getPenasltyAdd());
        penalty.put("PENALTYSUM", adpDocs.getPenaltySum());
        setPenalty(penalty);

        setDCancel(adpDocs.getDCancel());
        setLicensePlate(adpDocs.getLicensePlate());
        setBrandName(adpDocs.getBrandName());

        Map pdd = new HashMap();
        pdd.put("ID", adpDocs.getPddId());
        pdd.put("NAME", adpDocs.getPdd());
        setPdd(pdd);

        setLastNameU(adpDocs.getLastNameU());
        setFirstNameU(adpDocs.getFirstNameU());
        setMiddleNameU(adpDocs.getMiddleNameU());
        setBirthday(adpDocs.getBirthday());
        setLastNameR(adpDocs.getLastNameR());
        setFirstNameR(adpDocs.getFirstNameR());
        setMiddleNameR(adpDocs.getMiddleNameR());

        Map document = new HashMap();
        document.put("SERIA", adpDocs.getSeria());
        document.put("NUMBER", adpDocs.getNumber());
        document.put("DOCDATE", adpDocs.getDateReg());
        setDocument(document);

        Map status = new HashMap();
        status.put("ID", adpDocs.getStatusId());
        status.put("NAME", adpDocs.getStatus());
        setStatus(status);

        Map mark = new HashMap();
        mark.put("ID", adpDocs.getMarkId());
        mark.put("NAME", adpDocs.getMarkName());
        setMark( mark);

        setPaidInfo(adpDocs.getPaidInfo());
        setClntId(adpDocs.getClntId());

        Map direction = new HashMap();
        direction.put("directionId", adpDocs.getDirectionId());
        direction.put("direction", adpDocs.getDirection());
        setDirection(direction);

        setMakeTypeId(adpDocs.getMakeTypeId());
        
        setIsAuto((adpDocs.getMakeTypeId() == 1024) ? 1 : 0);
        setProcessStatus(getJsonProcessStatus(adpDocs.getProcessStatus()));
        setRegion(adpDocs.getRegionId());
        
		this.carId = adpDocs.getCarId();
	}
	private Map getJsonProcessStatus(String processStatus) {
        Gson gSon = new Gson();
        Map<String, Object> processStatusMap = 
                gSon.fromJson(String.valueOf(processStatus), Map.class);
        return processStatusMap;
    }
	
}
