package com.dkey.idris.entities.dto.clnt;

import com.dkey.idris.entities.dict.adds.CountryDict;
import com.dkey.idris.entities.dict.clnt.ClientPersonTypeDict;
import com.dkey.idris.entities.dict.dir.OrganizationTypeDict;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Legal extends ClientDomain {

    private String clientId;
    private OrganizationTypeDict organizationTypeDict;
    private String name;
    private String shortName;
    private String edrpou;
    private ClientPersonTypeDict clientPersonTypeDict;
    private CountryDict countryDict;
    private String innMain;

    public Legal() {
    }

    public Legal(ClientDomain clientDomain) {
        this.clientId = clientDomain.getClientId();
        this.organizationTypeDict = clientDomain.getOrganizationType();
        this.name = clientDomain.getName1U();
        this.shortName = clientDomain.getName2U();
        this.edrpou = clientDomain.getInnChar();
        this.clientPersonTypeDict = clientDomain.getPersonType();
        this.innMain = clientDomain.getInnMain();
        this.countryDict = clientDomain.getCountry();
    }
}
