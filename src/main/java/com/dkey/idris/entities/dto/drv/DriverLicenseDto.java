package com.dkey.idris.entities.dto.drv;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dkey.idris.entities.domain.drv.DriverLicence;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DriverLicenseDto {
    
    private Long paperId;
    private Long clientId;
    private Long clientVerId;
    private String lNameUA;
    private String fNameUA;
    private String mNameUA;
    private String lNameUS;
    private String fNameUS;
    private List<Map<String,String>> categories;
    private String sDoc;
    private String nDoc;
    private Long ownerCode;
    private LocalDate birthday;
    private LocalDate dDoc;
    private LocalDate dEnd;
    private String addressUA;
    private String addressUS;
    private byte[] photo;
    private String depUA;
    private String depUS;
    private Map status;

    public DriverLicenseDto(DriverLicence driverLicence){

        this.paperId = driverLicence.getPaperId();
        this.categories = driverLicence.getCategories();
        this.sDoc = driverLicence.getSeria();
        this.nDoc = driverLicence.getNumber();
        this.dDoc = driverLicence.getDateIssued();
        this.dEnd = driverLicence.getDateEnd();
        this.depUA = driverLicence.getDepUA();
        this.depUS = driverLicence.getDepUS();

        Map status = new HashMap();
        status.put("statusId", driverLicence.getStatusId());
        status.put("status", driverLicence.getStatus());
        this.status = status;

        this.photo = driverLicence.getPhoto();
        this.addressUA = driverLicence.getAddressUA();
        this.addressUS = driverLicence.getAddressUS();
        this.birthday = driverLicence.getBirthday();
        this.ownerCode = driverLicence.getInn();
        this.clientId = driverLicence.getClientId();
        this.clientVerId = driverLicence.getClientVerId();
        this.lNameUA = driverLicence.getLNameUA();
        this.fNameUA = driverLicence.getFirstNameUA();
        this.mNameUA = driverLicence.getMiddleNameUA();
        this.lNameUS = driverLicence.getLastNameUS();
        this.fNameUS = driverLicence.getFNameUS();

    }
}
