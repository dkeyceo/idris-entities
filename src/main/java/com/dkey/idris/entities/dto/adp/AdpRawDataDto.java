package com.dkey.idris.entities.dto.adp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AdpRawDataDto implements Serializable {

    private String id;
    private String file;
}
