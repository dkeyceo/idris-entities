package com.dkey.idris.entities.dto.adds;

import com.dkey.idris.entities.domain.adds.clnt.ClientAddressDomain;
import com.dkey.idris.entities.utils.RegionsKoatuu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientAddressDIIADto {

    private Long claddrId;
    private Long clientId;
    private Map addressType;
    private String isReal;
    private Long addressId;
    private Map city;
    private Map cityType;
    private String region;
    private String distr;
    private String distrCity;
    private Map streetType;
    private String street;
    private String postalCode;
    private String nHouse;
    private String sHouse;
    private String corps;
    private String nFlat;
    private String sFlat;
    private String isDirty;
    private String address;
    private Map country;

    public ClientAddressDIIADto(ClientAddressDomain addr) {
        this.claddrId = Long.parseLong(addr.getCladdrId());
        this.clientId = Long.parseLong(addr.getClientId());
        handleAddressType(addr);
        this.isReal = addr.getIsReal();
        this.addressId = Long.parseLong(addr.getAddress().getAddressId());
        handleCity(addr);
        handleCityType(addr);
        handleRegion(addr);
        handleStreetType(addr);
        this.street = addr.getAddress().getStreet();
        this.postalCode = addr.getAddress().getPostalCode();
        this.nHouse = addr.getAddress().getNHouse();
        this.sHouse = addr.getAddress().getSHouse();
        this.nFlat = addr.getAddress().getNFlat();
        this.sFlat = addr.getAddress().getSFlat();
        this.isDirty = addr.getAddress().getIsDirty();
        this.address = addr.getAddress().getAddress();

        Map country = new HashMap();
        country.put("ID", addr.getAddress().getCountry().getId());
        country.put("VALUE", addr.getAddress().getCountry().getValue());
        this.country = country;
    }

    private void handleRegion(ClientAddressDomain addr) {
        if (addr.getAddress().getCity() != null) {
            this.region = RegionsKoatuu.getValueById(addr
                    .getAddress().getCity().getKoatuuId().substring(0, 2));
        }
    }

    private void handleAddressType(ClientAddressDomain addr) {
        Map addrType = new HashMap();
        if (addr.getAddressType() != null) {
            addrType.put("ID", addr.getAddressType().getId());
            addrType.put("VALUE", addr.getAddressType().getValue());
            this.addressType = addrType;
        } else {
            addrType.put("ID", "");
            addrType.put("VALUE", "");
            this.addressType = addrType;
        }
    }

    private void handleCity(ClientAddressDomain addr) {
        Map city = new HashMap();
        if (addr.getAddress().getCity() != null) {
            city.put("ID", addr.getAddress().getCity().getCityId());
            city.put("VALUE", addr.getAddress().getCity().getCityName());
            this.city = city;
        } else {
            city.put("ID", "");
            city.put("VALUE", "");
            this.city = city;
        }
    }

    private void handleCityType(ClientAddressDomain addr) {
        Map cityType = new HashMap();
        if (addr.getAddress().getCity().getCityType() != null) {
            cityType.put("ID", addr.getAddress().getCity().getCityType().getId());
            cityType.put("VALUE", addr.getAddress().getCity().getCityType().getValue());
            this.cityType = cityType;
        } else {
            cityType.put("ID", "");
            cityType.put("VALUE", "");
            this.cityType = cityType;
        }
    }

    private void handleStreetType(ClientAddressDomain addr) {
        Map streetType = new HashMap();
        if (addr.getAddress().getStreetType() != null) {
            streetType.put("ID", addr.getAddress().getStreetType().getId());
            streetType.put("VALUE", addr.getAddress().getStreetType().getValue());
            this.streetType = streetType;
        } else {
            streetType.put("ID", "");
            streetType.put("VALUE", "");
            this.streetType = streetType;
        }
    }

}
