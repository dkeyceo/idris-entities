package com.dkey.idris.entities.dto.clnt.doc;

import com.dkey.idris.entities.domain.car.CarDocDomain;
import com.dkey.idris.entities.domain.car.CarDocPaperDomain;
import com.dkey.idris.entities.dto.car.CarSprDiiaDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SprCertificateDiiaDto {

    private ClientSprDiiaDto regSertifDoc;
    private CarSprDiiaDto regSertifCar;

    public SprCertificateDiiaDto(ClientSprDiiaDto regSertifDoc){
        this.regSertifDoc = regSertifDoc;
    }

    public SprCertificateDiiaDto(CarSprDiiaDto regSertifCar){
        this.regSertifCar = regSertifCar;
    }

}
