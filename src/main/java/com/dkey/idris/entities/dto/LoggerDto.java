package com.dkey.idris.entities.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
public class LoggerDto implements Serializable {

    private String requestId;
    private LocalDateTime requestTime;
    private String userName;
    private String uri;
    private String requestParam;

    private String resultText;
    private Integer resultCode;
    private LocalDateTime responseTime;
    private String serviceName;

    public LoggerDto() {
        this.requestId = getRandomId();
        this.requestTime = LocalDateTime.now();
    }

    private String getRandomId() {
        String pattern = "yyyyMMddhHHmmssSSS";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(new Date());
    }

    public void setResponseTime() {
        this.responseTime = LocalDateTime.now();
    }

}
