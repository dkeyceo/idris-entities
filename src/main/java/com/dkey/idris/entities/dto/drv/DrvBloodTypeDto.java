package com.dkey.idris.entities.dto.drv;

import com.dkey.idris.entities.dict.drv.DrvBloodType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrvBloodTypeDto {

    private Long id;
    private String code;
    private String value;

    public DrvBloodTypeDto(DrvBloodType drvBloodType) {
        this.id = drvBloodType.getId();
        this.code = drvBloodType.getCode();
        this.value = drvBloodType.getValue();
    }

}
