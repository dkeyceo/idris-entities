package com.dkey.idris.entities.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.MultiValueMap;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponseIdris {
    private String requestId;
    private String responseId;
    private LocalDateTime responseDateTime;
    private MultiValueMap<String,Object> responseBody;
}