package com.dkey.idris.entities.http.adds;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestBodyAddressRegEdocGenerate {
	@NotNull
	@JsonProperty("KOATUU")
	private String koatuu;
	@NotNull
	@JsonProperty("STREET_NAME")
	private String streetName;
	@NotNull
	@JsonProperty("N_HOUSE")
	private String nHouse;
	@JsonProperty("S_HOUSE")
	private String sHouse;
	@JsonProperty("CORPS")
	private String corps;
	@JsonProperty("N_FLAT")
	private String nFlat;
	@JsonProperty("S_FLAT")
	private String sFlat;
	@JsonProperty("POSTAL_CODE")
	@NotNull
	@Max(5)
	private String postalCode;
	
}
