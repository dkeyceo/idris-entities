package com.dkey.idris.entities.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.MultiValueMap;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseRequestIdris {
    private String requestId;
    private Map<String, String> requestType;
    private LocalDateTime requestDateTime;
    private MultiValueMap<String,Object> requestBody;
    private String author;
}
