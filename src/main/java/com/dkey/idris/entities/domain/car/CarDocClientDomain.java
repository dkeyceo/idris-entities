package com.dkey.idris.entities.domain.car;

import com.dkey.idris.entities.dict.clnt.ClientAmpluaTypeDict;
import com.dkey.idris.entities.dict.clnt.ClientLinkTypeDict;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import com.dkey.idris.entities.domain.clnt.doc.ClientDocBaseDomain;
import com.dkey.idris.entities.domain.clnt.doc.ClientDocDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="ps_dai$car_docs_clnt")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDocClientDomain implements Serializable {
    @Id
    @Column(name="doc#r")
    private String docId;
    @Id
    @ManyToOne
    @JoinColumn(name="amplua#r", insertable = false, updatable = false)
    private ClientAmpluaTypeDict amplua;
    @Id
    @ManyToOne
    @JoinColumn(name="clnt#r", insertable = false, updatable = false)
    private ClientDomain client;
    @Column(name="CLNT_VER#R")
    private String clientVerId;
    @Column(name="is_actual")
    private String isActual;

    @ManyToOne
    @JoinColumn(name="link#r")
    private ClientLinkTypeDict clientLinkType;
    @ManyToOne
    @JoinColumn(name="paper#r", insertable = false, updatable = false)
    private ClientDocDomain document;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="doc#r", insertable = false, updatable = false)
    private CarDocDomain carDocs;
}
