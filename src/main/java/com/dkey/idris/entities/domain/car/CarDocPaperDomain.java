package com.dkey.idris.entities.domain.car;

import com.dkey.idris.entities.domain.clnt.doc.ClientDocBaseDomain;
import com.dkey.idris.entities.domain.clnt.doc.ClientDocDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name="PS_DAI$CAR_DOCS_PAPER")
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(CarDocPaperDomainPK.class)
public class CarDocPaperDomain implements Serializable {
    @Id
    @Column(name="doc#r")
    private String docId;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="doc#r",insertable=false, updatable=false)
    private CarDocDomain carDocs;
    @Column(name="doc#whs")
    private String docWhs;
    @Column(name="doc#whs_res")
    private String docWhsRes;
    private String isIdentity;
    @Id
    private String isNew;
    private String isTitle;
    @Column(name="paper#r")
    private String paperId;
    @Id
    @ManyToOne
    @JoinColumn(name = "paper#r", insertable = false, updatable = false)
    private ClientDocDomain clientDoc;
}
