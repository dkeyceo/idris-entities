package com.dkey.idris.entities.domain.drv;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name="driver_licences")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrvLicsClntIdOnly implements Serializable {
    @Id
    @Column(name="paper#")
    private Long paperId;
    @Column(name="clnt#")
    private Long clientId;
    @Column(name="s_doc")
    private String seria;
    @Column(name="n_doc")
    private String number;
    @Column(name="d_doc")
    private LocalDate dateIssued;

}

