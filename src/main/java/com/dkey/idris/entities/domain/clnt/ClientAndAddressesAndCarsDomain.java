package com.dkey.idris.entities.domain.clnt;

import com.dkey.idris.entities.dict.adds.CountryDict;
import com.dkey.idris.entities.dict.clnt.ClientGenderTypeDict;
import com.dkey.idris.entities.dict.clnt.ClientPersonTypeDict;
import com.dkey.idris.entities.dict.dir.OrganizationTypeDict;
import com.dkey.idris.entities.domain.adds.clnt.ClientAddressDomain;
import com.dkey.idris.entities.domain.car.CarDocDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "ps_dai$clients")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientAndAddressesAndCarsDomain extends BaseDomainClient{

    @ManyToOne
    @JoinColumn(name="org#r", insertable = false, updatable = false)
    private OrganizationTypeDict organizationType;
    @Column(name="obj#r")
    private Long objId;
    @ManyToOne
    @JoinColumn(name="country#r", insertable = false, updatable = false)
    private CountryDict country;
    @Column(name = "name1_u")
    private String name1U;
    @Column(name = "name2_u")
    private String name2U;
    @Column(name = "name3_u")
    private String name3U;
    @Column(name = "name1_l")
    private String name1L;
    @Column(name = "name2_l")
    private String name2L;
    @Column(name = "name3_l")
    private String name3L;
    @Column(name = "birthday")
    private LocalDate birthDay;
    @ManyToOne
    @JoinColumn(name="sex", insertable = false, updatable = false)
    private ClientGenderTypeDict genderType;
    @ManyToOne
    @JoinColumn(name="person", insertable = false, updatable = false)
    private ClientPersonTypeDict personType;
    @Column(name = "inn")
    private String inn;
    @Column(name = "inn#main")
    private String innMain;
    @Column(name = "pin")
    private String pin;
    @Column(name = "INN_CHAR")
    private String innChar;
    @Column(name = "NO_INN")
    private String noInn;
    @Column(name="is_hidden")
    private String isHidden;
    @Column(name="CREATED_BY")
    private String createdBy;
    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;
    @Column(name="LAST_UPDATED_BY")
    private String lastUpdateBy;
    @Column(name = "LAST_UPDATE_DATE")
    private LocalDate lastUpdateDate;

    @OneToMany(cascade= CascadeType.ALL)
    @JoinColumn(name = "clnt#r")
    private List<ClientAddressDomain> clientAddresses;
    @OneToMany(cascade= CascadeType.ALL)
    @JoinColumn(name = "clnt#r")
    private List<CarDocDomain> cars;
}
