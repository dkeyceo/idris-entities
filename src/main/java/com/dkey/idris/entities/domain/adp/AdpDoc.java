package com.dkey.idris.entities.domain.adp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Table(name = "adp_doc")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDoc implements Serializable {

    @Id
    @Column(name = "DOC_ID")
    private String docId;
    @Column(name = "D_SYSTEM")
    private LocalDate dSystem;
    @Column(name = "LAST_UPDATE_DATE")
    private LocalDate lastUpdateDate;
    @Column(name = "DEP_ID")
    private String depId;
    @Column(name = "DEP")
    private String dep;
    @Column(name = "D_REG")
    private LocalDate dateReg;
    @Column(name = "FABULA")
    private String fabula;
    @Column(name = "KUPAP_ID")
    private String kupapId;
    @Column(name = "KUPAP")
    private String kupap;
    @Column(name = "D_FAIL")
    private LocalDateTime dFail;
    @Column(name = "OB")
    private String ob;
    @Column(name = "RN")
    private String rn;
    @Column(name = "NS")
    private String ns;
    @Column(name = "UL")
    private String ul;
    @Column(name = "ROAD_KM")
    private String roadKm;
    @Column(name = "SEND_ID")
    private String sendId;
    @Column(name = "SEND")
    private String send;
    @Column(name = "DEP_CREATED_ID")
    private String depCreatedId;
    @Column(name = "DEP_CREATED")
    private String depCreated;
    @Column(name = "DECISION_ID")
    private String decisionId;
    @Column(name = "DECISION")
    private String decision;
    @Column(name = "D_PENALTY")
    private LocalDate dPenalty;
    @Column(name = "PENALTY_ID")
    private String penaltyId;
    @Column(name = "PENALTY")
    private String penalty;
    @Column(name = "N_TERM")
    private String nTerm;
    @Column(name = "PAY_TERM")
    private String payTerm;
    @Column(name = "D_PAY_TERM")
    private LocalDate dPayTerm;
    @Column(name = "PENALTY_ADD_ID")
    private String penaltyAddId;
    @Column(name = "PENALTY_ADD")
    private String penasltyAdd;
    @Column(name = "D_CANCEL")
    private LocalDate dCancel;
    @Column(name = "N_REG")
    private String licensePlate;
    @Column(name = "BRAND")
    private String brandName;
    @Column(name = "PDD_ID")
    private String pddId;
    @Column(name = "PDD")
    private String pdd;
    @Column(name = "NAME1_U")
    private String lastNameU;
    @Column(name = "NAME2_U")
    private String firstNameU;
    @Column(name = "NAME3_U")
    private String middleNameU;
    @Column(name = "BIRTHDAY")
    private LocalDate birthday;
    @Column(name = "NAME1_R")
    private String lastNameR;
    @Column(name = "NAME2_R")
    private String firstNameR;
    @Column(name = "NAME3_R")
    private String middleNameR;
    @Column(name = "S_DOC")
    private String seria;
    @Column(name = "N_DOC")
    private Long number;
    @Column(name = "STATUS_ID")
    private String statusId;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "MARK_ID")
    private String markId;
    @Column(name = "MARK")
    private String markName;
    @Column(name = "PAID_INFO")
    private String paidInfo;
    @Column(name = "CLNT_ID")
    private Long clntId;
    @Column(name = "maketype_id")
    private Long makeTypeId;
    @Column(name = "address")
    private String address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "adpDoc")
    private Collection<AdpDocProcessStatus> processStatus;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "adpDoc")
    private Collection<AdpDocMove> move;
}
