package com.dkey.idris.entities.domain.adp;

import com.dkey.idris.entities.dict.adp.AdpImportStatusDict;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="ps_dai$adp_fisal_data")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpFiscalDataDomain {
    @Id
    @Column(name="payment#")
    private String paymentId;
    private LocalDate fdat;
    private String nmk;
    private String nls;
    private String nms;
    private String ostf;
    private String s;
    private String nd;
    private String dk;
    private String ref;
    private String tt;
    private String nazn;
    private LocalDate dapp;
    private LocalDate datp;
    private String nlsk;
    private String mfo;
    private String nb;
    private String namk;
    private String okpo;
    @ManyToOne
    @JoinColumn(name="status#r", insertable = false, updatable = false)
    private AdpImportStatusDict importStatus;
    @ManyToOne
    @JoinColumn(name="DOCTERM#R", insertable = false, updatable = false)
    @JsonIgnore
    private AdpDocTermDomain adpDocTermDomain;

}
