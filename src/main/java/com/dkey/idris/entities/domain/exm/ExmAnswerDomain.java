package com.dkey.idris.entities.domain.exm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="ps_dai$exm_answers")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExmAnswerDomain {
    @Id
    @Column(name="answer#")
    private String answerId;
    @ManyToOne
    @JoinColumn(name="quest#r", insertable = false, updatable = false)
    @JsonIgnore
    private ExmQuestionDomain question;
    private int answerNum;
    private String isCorrect;
    private String answersName;
}
