package com.dkey.idris.entities.domain.doc;

import com.dkey.idris.entities.dict.doc.DocOperDict;
import com.dkey.idris.entities.dict.doc.DocStatusDict;
import com.dkey.idris.entities.dict.doc.DocTypeDict;
import com.dkey.idris.entities.dict.stf.DepartmentDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="ps_dai$docs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Doc {
    @Id
    @Column(name="doc#")
    private Long docId;
    @Column(name="doc#main")
    private Long docMain;
    @ManyToOne
    @JoinColumn(name="doctype#r", insertable = false, updatable = false)
    private DocTypeDict docType;
    @ManyToOne
    @JoinColumn(name="status#r", insertable = false, updatable = false)
    private DocStatusDict docStatus;
    @ManyToOne
    @JoinColumn(name="oper#r", insertable = false, updatable = false)
    private DocOperDict docOper;
    @ManyToOne
    @JoinColumn(name="dep#r", insertable = false, updatable = false)
    private DepartmentDict department;
    @ManyToOne
    @JoinColumn(name="dep#from", insertable = false, updatable = false)
    private DepartmentDict departmentFrom;
    @ManyToOne
    @JoinColumn(name="dep#to", insertable = false, updatable = false)
    private DepartmentDict departmentTo;
    @Column(name="d_work")
    private LocalDate dWork;
    @Column(name="d_system")
    private LocalDate dSystem;
    @Column(name="n_doc")
    private String nDoc;
    @Column(name="d_doc")
    private LocalDate dDoc;
    @Column(name="user#r")
    private String userId;
    @Column(name="note")
    private String note;
    @Column(name="created_by")
    private String createdBy;
    @Column(name="CREATION_DATE")
    private LocalDate creationDate;
    @Column(name="LAST_UPDATED_BY")
    private String lastUpdateBy;
    @Column(name="LAST_UPDATE_DATE")
    private LocalDate lastUpdateDate;

}
