package com.dkey.idris.entities.domain.adp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "adp_request_to_doc")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpReqToDoc {
    
    @Id
    @Column(name = "REQUESTID")
    private Long requestId;
    @Column(name = "DOC#R")
    private Long docId;
    @Column(name = "S_DOC")
    private String seria;
    @Column(name = "N_DOC")
    private Long number;
    
}
