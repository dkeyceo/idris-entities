package com.dkey.idris.entities.domain.adds;

import com.dkey.idris.entities.dict.adds.AddsStreetTypeDict;
import com.dkey.idris.entities.dict.adds.CountryDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="ps_dai$adds_street")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddsStreetDomain {
    @Id
    @Column(name="street#")
    private String streetId;
    private String street;
    private String isUsed;
    @ManyToOne
    @JoinColumn(name="tstreet#r", insertable = false, updatable = false)
    private AddsStreetTypeDict streetType;
    @ManyToOne
    @JoinColumn(name="city#r", insertable = false, updatable = false)
    private AddsCityDomain city;
}
