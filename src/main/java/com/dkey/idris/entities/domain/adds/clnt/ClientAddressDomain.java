package com.dkey.idris.entities.domain.adds.clnt;

import com.dkey.idris.entities.dict.adds.AddsTypeDict;
import com.dkey.idris.entities.dict.adds.CountryDict;
import com.dkey.idris.entities.domain.adds.AddressDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="ps_dai$clnt_addr")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientAddressDomain {
    @Id
    @Column(name="claddr#")
    private String claddrId;
    @Column(name="clnt#r")
    private String clientId;
    @ManyToOne( optional = true)
    @JoinColumn(name="tadds#r", insertable = false, updatable = false)
    private AddsTypeDict addressType;
    @ManyToOne
    @JoinColumn(name="address#r", insertable = false, updatable = false)
    private AddressDomain address;
    private LocalDate dBegin;
    private LocalDate dEnd;
    private String isReal;
}
