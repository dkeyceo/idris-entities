package com.dkey.idris.entities.domain.adp;

import com.dkey.idris.entities.dict.adp.*;
import com.dkey.idris.entities.dict.stf.DepartmentDict;
import com.dkey.idris.entities.domain.adds.AddressDomain;
import com.dkey.idris.entities.domain.car.CarDomain;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "ps_dai$adp_docs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDocDomain {

    @Id
    @Column(name="doc#r")
    private String docId;
    @ManyToOne
    @JoinColumn(name="address#fail", insertable = false, updatable = false)
    private AddressDomain addressFail;
    @ManyToOne
    @JoinColumn(name="car#r", insertable = false, updatable = false)
    private CarDomain car;
    @ManyToOne
    @JoinColumn(name="clnt#car", insertable = false, updatable = false)
    private ClientDomain owner;
    @ManyToOne
    @JoinColumn(name="clnt#r", insertable = false, updatable = false)
    private ClientDomain offender;
    @ManyToOne
    @JoinColumn(name="DEP#CREATED", insertable = false, updatable = false)
    private DepartmentDict departmentCreated;
    @ManyToOne
    @JoinColumn(name="DEP#PRO", insertable = false, updatable = false)
    private DepartmentDict departmentDecision;
    @Column(name="d_fail")
    private LocalDateTime dFail;
    private LocalDate dReg;
    private String fabula;
    @ManyToOne
    @JoinColumn(name="INTR_SND_MSG_STATUS#R",insertable = false, updatable = false)
    private AdpProcessStatusDict sendStatus;
    @ManyToOne
    @JoinColumn(name="INTRUDER#R", insertable = false, updatable = false)
    private AdpIntruderDict intruderType;
    @ManyToOne
    @JoinColumn(name="KUPAP#R", insertable = false, updatable = false)
    private AdpKupapDict kupap;
    @ManyToOne
    @JoinColumn(name="MAKETYPE#R", insertable = false, updatable = false)
    private AdpMakeTypeDict makeType;
    @Column(name="n_doc")
    private String nDoc;
    @Column(name="s_doc")
    private String sDoc;
    @ManyToOne
    @JoinColumn(name="PDD#R", insertable = false, updatable = false)
    private AdpPdrDict pdr;
    @OneToMany(mappedBy = "adpDocDomain")
    private List<AdpDocMoveDomain> moveDocuments;
    @OneToMany(mappedBy = "adpDocDomain")
    private List<AdpDocTermDomain> termDocuments;
    @OneToMany(mappedBy = "adpDocDomain")
    private List<AdpDocProcessStatusDomain> processStatuses;

}
