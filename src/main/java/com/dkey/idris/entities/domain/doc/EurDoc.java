package com.dkey.idris.entities.domain.doc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$eur_docs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EurDoc {
    @Id
    @Column(name="doc#r")
    private Long docId;
    @Column(name="clnt#r")
    private Long clntId;
    @Column(name="REQUESTID#EXT")
    private Long requestId;
    @Column(name="ret_note")
    private String rejectReason;
}
