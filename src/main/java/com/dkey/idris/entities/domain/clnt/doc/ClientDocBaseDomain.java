package com.dkey.idris.entities.domain.clnt.doc;


import com.dkey.idris.entities.dict.adds.CountryDict;
import com.dkey.idris.entities.dict.clnt.doc.ClientDocStatusDict;
import com.dkey.idris.entities.dict.clnt.doc.ClientDocTypeDict;
import com.dkey.idris.entities.dict.stf.DepartmentDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@MappedSuperclass
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientDocBaseDomain {
    @Id
    @Column(name="paper#")
    private String paperId;
    @Column(name="clnt#r")
    private String clntId;
    @ManyToOne(optional = true)
    @JoinColumn(name="tdoc#r", insertable = false, updatable = false)
    private ClientDocTypeDict docType;
    @ManyToOne(optional = true)
    @JoinColumn(name="country#r", insertable = false, updatable = false)
    private CountryDict countryDict;
    @Column(name="s_doc")
    private String sDoc;
    @Column(name="n_doc")
    private String nDoc;
    @Column(name="d_doc")
    private LocalDate dDoc;
    @Column(name="d_end")
    private LocalDate dEnd;
    @ManyToOne(optional = true)
    @JoinColumn(name="dep#out", insertable = false, updatable = false)
    private DepartmentDict departmentDict;
    @Column(name="clnt#out")
    private Long clntOut;
    @ManyToOne(optional = true)
    @JoinColumn(name="status#r", insertable = false, updatable = false)
    private ClientDocStatusDict docStatus;
    @Column(name="stand_out")
    private String standOut;
    @Column(name="is_real")
    private String isReal;
    @Column(name="created_by")
    private String createdBy;
    @Column(name="CREATION_DATE")
    private LocalDate creationDate;
    @Column(name="LAST_UPDATED_BY")
    private String lastUpdateBy;
    @Column(name="LAST_UPDATE_DATE")
    private LocalDate lastUpdateDate;
    @Column(name="ree#out")
    private Long reeOut;
}
