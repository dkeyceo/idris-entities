package com.dkey.idris.entities.domain.adp;

import com.dkey.idris.entities.dict.adds.CountryDict;
import com.dkey.idris.entities.dict.adp.AdpDirectDict;
import com.dkey.idris.entities.dict.doc.DocOperDict;
import com.dkey.idris.entities.dict.stf.DepartmentDict;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="ps_dai$adp_docs_move")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDocMoveDomain {
    @Id
    @Column(name="ADPMOVE#")
    private String adpMoveId;
    @ManyToOne
    @JoinColumn(name="DOC#R" , insertable = true, updatable = false)
    @JsonIgnore
    private AdpDocDomain adpDocDomain;
    @ManyToOne
    @JoinColumn(name="oper#r", insertable = false, updatable = false)
    private DocOperDict docOper;
    @ManyToOne
    @JoinColumn(name="DIRECT#R", insertable = false, updatable = false)
    private AdpDirectDict direction;
    private String nDirect;
    private LocalDate dDirect;
    @ManyToOne
    @JoinColumn(name="dep#from", insertable = false, updatable = false)
    private DepartmentDict departmentFrom;
    @ManyToOne
    @JoinColumn(name="dep#to", insertable = false, updatable = false)
    private DepartmentDict departmentTo;
    @ManyToOne
    @JoinColumn(name="clnt#to", insertable = false, updatable = false)
    private ClientDomain client;
    @ManyToOne
    @JoinColumn(name="country#to", insertable = false, updatable = false)
    private CountryDict country;
}
