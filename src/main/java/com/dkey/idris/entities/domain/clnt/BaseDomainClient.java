package com.dkey.idris.entities.domain.clnt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseDomainClient {
    @Id
    @Column(name = "clnt#")
    private String clientId;

}
