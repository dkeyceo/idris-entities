package com.dkey.idris.entities.domain.car;

import com.dkey.idris.entities.dict.car.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name="ps_dai$cars")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDomain {

    @Id
    @Column(name="car#")
    private String carId;
    private String vin;
    @Column(name="n_body")
    private String nBody;
    @Column(name="n_chassis")
    private String nChassis;
    @Column(name="n_engine")
    private String nEngine;
    @Column(name="d_reg")
    private LocalDate dFirstRegistration;
    @Column(name="own_weight")
    private Long ownWeight;
    @Column(name="total_weight")
    private Long totalWeight;
    @ManyToOne
    @JoinColumn(name="kind#r", insertable = false, updatable = false)
    private CarKindDict carKind;
    @ManyToOne
    @JoinColumn(name="body#r", insertable = false, updatable = false)
    private CarBodyDict carBody;
    @ManyToOne
    @JoinColumn(name="purpose#r", insertable = false, updatable = false)
    private CarPurposeDict carPurpose;
    @ManyToOne
    @JoinColumn(name="rank#r", insertable = false, updatable = false)
    private CarRankDict carRank;
    @ManyToOne
    @JoinColumn(name="color#r", insertable = false, updatable = false)
    private CarColorDict carColor;
    @ManyToOne
    @JoinColumn(name="fuel#r", insertable = false, updatable = false)
    private CarFuelDict carFuel;

    @ManyToOne
    @JoinColumn(name="model#r", insertable = false, updatable = false)
    private CarModelDict carModel;

    @ManyToOne
    @JoinColumn(name="brand#r", insertable = false, updatable = false)
    private CarBrandDict carBrand;
    private Long cylinder;
    private Long capacity;
    @Column(name="n_seating")
    private Long nSeating;
    @Column(name="n_standup")
    private Long nStandup;
    private String note;
    @Column(name="make_year")
    private Long makeYear;

    @OneToMany(mappedBy = "car")
    private List<CarDocDomain> carDocs;
}
