package com.dkey.idris.entities.domain.clnt.doc;

import com.dkey.idris.entities.dict.clnt.doc.DriverLicenseCategoryDict;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name="ps_dai$drv_card_cat")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DriverLicenseCategoryDomain implements Serializable {
    @Id
    @Column(name="paper#r")
    private String paperId;
    @Id
    @ManyToOne
    @JoinColumn(name="cat#r", insertable = false, updatable = false)
    private DriverLicenseCategoryDict category;
    @Column(name="d_open")
    private LocalDate dOpen;
    @Column
    private String experience;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="paper#r",insertable=false, updatable=false)
    private DriverLicenseDomain driverLicenseDomain;
}
