package com.dkey.idris.entities.domain.adp;

import com.dkey.idris.entities.dict.adp.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name="ps_dai$adp_docs_term")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDocTermDomain {
    @Id
    @Column(name="docterm#")
    private String doctermId;
    @ManyToOne
    @JoinColumn(name="TRIBUTE#R" , insertable = true, updatable = false)
    private AdpTributeDict tribute;
    @ManyToOne
    @JoinColumn(name="CAUSERESULT#R" , insertable = true, updatable = false)
    private AdpCauseResultDict causeResult;
    @ManyToOne
    @JoinColumn(name="DECISION#R" , insertable = true, updatable = false)
    private AdpDecisionDict decision;
    @ManyToOne
    @JoinColumn(name="DOC#R" , insertable = true, updatable = false)
    @JsonIgnore
    private AdpDocDomain adpDocDomain;
    private LocalDate dPenalty;
    private String isReal;
    private String nTerm;
    private String nTermAdd;
    @ManyToOne
    @JoinColumn(name="PENALTY#R" , insertable = true, updatable = false)
    private AdpPenaltyDict penalty;
    @ManyToOne
    @JoinColumn(name="STATUS#R" , insertable = true, updatable = false)
    private AdpTermImportDict termImport;

    @OneToMany(mappedBy = "adpDocTermDomain")
    private List<AdpFiscalDataDomain> payList;

}
