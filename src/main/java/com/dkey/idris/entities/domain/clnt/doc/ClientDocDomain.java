package com.dkey.idris.entities.domain.clnt.doc;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ps_dai$clnt_docs")
@Data
public class ClientDocDomain extends ClientDocBaseDomain {
}
