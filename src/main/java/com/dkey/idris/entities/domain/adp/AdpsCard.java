package com.dkey.idris.entities.domain.adp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "adps_card_info")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpsCard implements Serializable {

    @Id
    @Column(name = "DDDOC_ID")
    private Long dddDocId;
    @Column(name = "DCDCAR_ID")
    private Long dcdCarId;
    @Column(name = "DCDBRAND_ID")
    private Long dcdBrandId;
    @Column(name = "DCDMODEL_ID")
    private Long dcdModelId;
    @Column(name = "DCDKIND_ID")
    private Long dcdKindId;
    @Column(name = "DCDRANK_ID")
    private Long dcdRankId;
    @Column(name = "DCDBODY_ID")
    private Long dcdBodyId;
    @Column(name = "DCDCOLOR_ID")
    private Long dcdColorId;
    @Column(name = "DCDCLNT_ID")
    private Long dcdClntId;
    @Column(name = "DCDCLNTVER_ID")
    private String dcdClntVerId;
    @Column(name = "PAPER#")
    private Long paperId;
    @Column(name = "DRV_ID")
    private String drvId;
    @Column(name = "R_ADD_ID")
    private Long rAddId;
    @Column(name = "TYPE")
    private String type;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "MIDDLE_NAME")
    private String middleName;
    @Column(name = "DATE_OF_BIRTH")
    private LocalDate dateOfBirth;
    @Column(name = "IPN")
    private String ipn;
    @Column(name = "NATIONALITY")
    private String nationality;
    @Column(name = "BIRTH_ADDRESS")
    private String birthAddress;
    @Column(name = "SEX")
    private String sex;
    @Column(name = "PROPER_USER")
    private String properUser;
    @Column(name = "PASSPORT_TYPE")
    private String passportType;
    @Column(name = "PASSPORT_SERIES")
    private String passportSeries;
    @Column(name = "PASSPORT_NUM")
    private String passportNum;
    @Column(name = "PASSPORT_AUTHORITY")
    private String passportAuthority;
    @Column(name = "PASSPORT_DATE")
    private String passportDate;
    @Column(name = "LIC_TYPE")
    private String licType;
    @Column(name = "LIC_SERIES")
    private String licSeries;
    @Column(name = "LIC_NUM")
    private String licNum;
    @Column(name = "LIC_AUTHORITY")
    private String licAuthority;
    @Column(name = "LIC_DATE")
    private String licDate;
    @Column(name = "LIC_DATE_OF_CANCELLATION")
    private String licDateOfCancelattion;
    @Column(name = "CATEGORIES")
    private String categories;
    @Column(name = "SPR_TYPE")
    private String sprType;
    @Column(name = "SPR_SERIES")
    private String sprSeries;
    @Column(name = "SPR_NUM")
    private String sprNum;
    @Column(name = "SPR_AUTHORITY")
    private String sprAuthority;
    @Column(name = "SPR_DATE")
    private String sprDate;
    @Column(name = "SPR_DATE_OF_CANCELLATION")
    private String sprDateOfCancelattion;
    @Column(name = "R_ADDRESS_NAME")
    private String rAddressName;
    @Column(name = "COUNTRY")
    private String country;
    @Column(name = "KOATUU")
    private String koatuu;
    @Column(name = "CITY")
    private String city;
    @Column(name = "TCITY_FULL")
    private String tCityFull;
    @Column(name = "R_TSTREET_FULL")
    private String rTstreetFull;
    @Column(name = "STREET")
    private String street;
    @Column(name = "N_HOUSE")
    private String nHouse;
    @Column(name = "APT")
    private String apt;
    @Column(name = "LICENSEPLATE")
    private String licensePlate;
    @Column(name = "VIN")
    private String vin;
    @Column(name = "BRAND")
    private String brand;
    @Column(name = "MODEL")
    private String model;
    @Column(name = "CAR_TYPE")
    private String carType;
    @Column(name = "MAKE_YEAR")
    private Long makeYear;
    @Column(name = "DATE_FIRST_REGISTRATION")
    private LocalDate dateFirstRegistration;
    @Column(name = "DATE_REGISTRATION")
    private LocalDate dateRegistration;
    @Column(name = "TSC_NAME")
    private String tscName;
    @Column(name = "TSC_NUMBER")
    private Long tscNumber;
    @Column(name = "TEXTREG")
    private String textReg;

}
