package com.dkey.idris.entities.domain.adp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "dai$adp_docs_move")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDocMove {
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "doc#r")
    @JsonIgnore
    private AdpDoc adpDoc;
    
    @Column(name = "OPER#R")
    private Long operId;
    @Column(name = "DIRECT#R")
    private Long directId;
    @Column(name = "DEP#FROM")
    private Long depFrom;
    @Column(name = "DEP#TO")
    private Long depTo;
    @Column(name = "CLNT#TO")
    private Long clntTo;
    @Column(name = "N_DIRECT")
    private String numDirect;
    @Column(name = "D_DIRECT")
    private LocalDate dateDirect;
    @Column(name = "COUNTRY#TO")
    private Long countryTo;
    @Column(name = "COUNTRY_DEP")
    private String countryDep;
    @JsonIgnore
    @Column(name = "CREATED_BY")
    private String createdBy;
    @JsonIgnore
    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;
    @JsonIgnore
    @Column(name = "LAST_UPDATED_BY")
    private String lastUpdBy;
    @JsonIgnore
    @Column(name = "LAST_UPDATE_DATE")
    private LocalDate lastUpdDate;
    
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)   
    @Column(name = "adpmove#")
    private Long adpMoveId;
    
}
