package com.dkey.idris.entities.domain.clnt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ps_dai$clnt_contacts")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientContactDomain implements Serializable {
    @Id
    @Column(name="CLNT#R")
    private String clientId;
    @Id
    @Column(name="TCONTACT#R")
    private String contactId;
    @Id
    @Column(name = "CONTACT_VALUE")
    private String contactValue;

}
