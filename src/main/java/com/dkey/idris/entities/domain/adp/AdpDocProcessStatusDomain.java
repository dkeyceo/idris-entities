package com.dkey.idris.entities.domain.adp;

import com.dkey.idris.entities.dict.adp.AdpProcessStatusDict;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="dai$adp_docs_process_status")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDocProcessStatusDomain {
    @Id
    @Column(name="docsts#")
    private String docstsId;
    @ManyToOne
    @JoinColumn(name="DOC#R" , insertable = true, updatable = false)
    @JsonIgnore
    private AdpDocDomain adpDocDomain;
    @Column(name="date_status")
    private LocalDate dateStatus;
    @ManyToOne
    @JoinColumn(name="PROCESS_STATUS#R",insertable = false, updatable = false)
    private AdpProcessStatusDict sendStatus;
    @Column(name="raw_doc")
    private String rawDoc;
    private String note;
}
