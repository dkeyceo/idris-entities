package com.dkey.idris.entities.domain.adds;

import com.dkey.idris.entities.dict.adds.AddsStreetTypeDict;
import com.dkey.idris.entities.dict.adds.CountryDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="ps_dai$address")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDomain {
    @Id
    @Column(name="address#")
    private String addressId;

    @ManyToOne( optional = true)
    @JoinColumn(name="country#r", insertable = false, updatable = false)
    private CountryDict country;

    @ManyToOne( optional = true)
    @JoinColumn(name="city#r", insertable = false, updatable = false)
    private AddsCityDomain city;

    @ManyToOne( optional = true)
    @JoinColumn(name="tstreet#r", insertable = false, updatable = false)
    private AddsStreetTypeDict streetType;

    @Column(name="postal_code")
    private String postalCode;
    private String street;
    private String nHouse;
    private String sHouse;
    private String corps;
    private String nFlat;
    private String sFlat;
    private String isDirty;
    private String address;
}
