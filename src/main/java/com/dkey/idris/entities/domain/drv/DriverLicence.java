package com.dkey.idris.entities.domain.drv;

import com.dkey.idris.entities.utils.CategoriesConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="driver_licences")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DriverLicence implements Serializable {
    @Id
    @Column(name="paper#")
    private Long paperId;
    @Column(name="clnt#")
    private Long clientId;
    @Column(name="clnt_ver_id")
    private Long clientVerId;
    @Column(name="name1_u")
    private String lNameUA;
    @Column(name="name2_u")
    private String firstNameUA;
    @Column(name="name3_u")
    private String middleNameUA;
    @Column(name="name1_l")
    private String lastNameUS;
    @Column(name="name2_l")
    private String fNameUS;
    @Convert(converter = CategoriesConverter.class)
    private List<Map<String,String>> categories;
    @Column(name="s_doc")
    private String seria;
    @Column(name="n_doc")
    private String number;
    @JsonIgnore
    private Long inn;
    private LocalDate birthday;
    @Column(name="d_doc")
    private LocalDate dateIssued;
    @Column(name="d_end")
    private LocalDate dateEnd;
    @Column(name="addressua")
    private String addressUA;
    @Column(name="addressus")
    private String addressUS;
    @Lob
    private byte[] photo;
    @Column(name="dep_name_print")
    private String depUA;
    @Column(name="dep_name_print_latin")
    private String depUS;
    @Column(name="status#r")
    private Long statusId;
    @Column(name="status")
    private String status;    
    
}

