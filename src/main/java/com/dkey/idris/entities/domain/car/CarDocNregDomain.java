package com.dkey.idris.entities.domain.car;

import com.dkey.idris.entities.dict.car.CarRegStatusDict;
import com.dkey.idris.entities.dict.car.CarRegTypeDict;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name="PS_DAI$CAR_DOCS_NREG")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDocNregDomain implements Serializable {

    @Id
    @Column(name="doc#r")
    private String docId;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="doc#r",insertable=false, updatable=false)
    private CarDocDomain carDocs;
    @Id
    @ManyToOne
    @JoinColumn(name="treg#r",insertable=false, updatable=false)
    private CarRegTypeDict carRegType;
    @ManyToOne
    @JoinColumn(name="regstat#r",insertable=false, updatable=false)
    private CarRegStatusDict carRegStatus;
    @Id
    @Column(name="n_reg")
    private String licensePlate;
    @Id
    @Column(name="is_new")
    private String isNew;
    @Id
    @Column(name="is_inz")
    private String isInz;
    @Id
    @Column(name="is_transit")
    private String isTransit;
}
