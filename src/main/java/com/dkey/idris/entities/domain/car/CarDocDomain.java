package com.dkey.idris.entities.domain.car;

import com.dkey.idris.entities.dict.car.CarEcologyDict;
import com.dkey.idris.entities.dict.doc.DocOperDict;
import com.dkey.idris.entities.dict.stf.DepartmentDict;
import com.dkey.idris.entities.domain.clnt.ClientDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "ps_dai$car_docs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDocDomain {

    @Id
    @Column(name="doc#r")
    private String docId;
    @OneToMany(mappedBy = "carDocs")
    private List<CarDocNregDomain> plates;
    @OneToMany(mappedBy = "carDocs")
    private List<CarDocPaperDomain> carDocs;
    @Column(name="d_reg")
    private LocalDate dateRegistration;
//    @ManyToOne
//    @JoinColumn(name="oper#r", insertable = false, updatable = false)
//    private DocOperDict operation;
    @Column(name="lm_oper")
    private String isActive;
    @ManyToOne
    @JoinColumn(name="dep_reg#r", insertable = false, updatable = false)
    private DepartmentDict department;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="car#r",insertable=false, updatable=false)
    private CarDomain car;

    @Column(name="note_prn")
    private String note;

    @ManyToOne
    @JoinColumn(name = "clnt#r", insertable = false,updatable = false)
    private ClientDomain owner;


    @Column(name="clnt_ver#r")
    private Long ownerVerId;
    @Column(name="kind#r")
    @JsonIgnore
    private Long kindId;
    @ManyToOne
    @JoinColumn(name="ecology#r", insertable = false, updatable = false)
    private CarEcologyDict carEcology;
    @Column(name="odometer")
    private Long odometer;
    @OneToMany(mappedBy = "carDocs")
    private List<CarDocClientDomain> clients;
}
