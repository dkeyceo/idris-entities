package com.dkey.idris.entities.domain.exm;

import com.dkey.idris.entities.dict.exm.ExmSetTypeDict;
import com.dkey.idris.entities.dict.exm.ExmTopicDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ps_dai$exm_quests")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExmQuestionDomain {
    @Id
    @Column(name="quest#")
    private String questId;
    @ManyToOne
    @JoinColumn(name = "tset#r", insertable = false, updatable = false)
    private ExmSetTypeDict exmSet;
    @ManyToOne
    @JoinColumn(name = "topic#r", insertable = false, updatable = false)
    private ExmTopicDict exmTopic;
    private String nQuest;
    private String questsName;
    @Lob
    private byte[] picture;
    @OneToMany(mappedBy = "question")
    private List<ExmAnswerDomain> answers;
}
