package com.dkey.idris.entities.domain.adds;

import com.dkey.idris.entities.dict.adds.AddsCityTypeDict;
import com.dkey.idris.entities.dict.adds.CountryDict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="ps_dai$adds_city")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddsCityDomain {
    @Id
    @Column(name="city#")
    private String cityId;
    @ManyToOne
    @JoinColumn(name="city#main", insertable = false, updatable = false)
    private AddsCityDomain cityMain;
    @ManyToOne
    @JoinColumn(name="country#r", insertable = false, updatable = false)
    private CountryDict country;
    @ManyToOne
    @JoinColumn(name="tcity#r", insertable = false, updatable = false)
    private AddsCityTypeDict cityType;
    @Column(name = "koatuu#r")
    private String koatuuId;
    private String cityFullName;
    private String cityName;
    private String isUsed;
}