package com.dkey.idris.entities.domain.clnt.doc;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "ps_dai$clnt_docs")
@Data
public class DriverLicenseDomain extends ClientDocBaseDomain {
    @OneToMany(mappedBy = "driverLicenseDomain")
    private List<DriverLicenseCategoryDomain> categories;
}
