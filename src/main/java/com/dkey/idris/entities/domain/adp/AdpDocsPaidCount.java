package com.dkey.idris.entities.domain.adp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table 
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDocsPaidCount {
    
    @Id
    @Column(name = "count_docs")
    private Long countDocs;
    @Column(name = "sum_paid")
    private Long sumPaid;
    
}
