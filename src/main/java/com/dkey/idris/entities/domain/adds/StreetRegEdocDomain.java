package com.dkey.idris.entities.domain.adds;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="PS_DAI$STREET_REG_EDOC")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StreetRegEdocDomain implements Serializable{
	
	@Id
	@Column(name="KOATUU")
	private String koatuu;
	@Column(name="CITY_ID")
	private String cityId; 
	@Column(name="CITY_NAME")
	private String cityName;
	@Column(name="REGION_ID")
	private String regionId;
	@Column(name="REGION_NAME")
	private String regionName;
	
	@Column(name="COUNTRY_ID")
	private String countryId;
	@Column(name="COUNTRY_NAME")
	private String countryName;
	
	@Column(name="CITY_TYPE_ID ")
	private String cityTypeId;
	@Column(name="CITY_TYPE_NAME")
	private String cityTypeName;
	
	@Column(name="STREET_TYPE_ID")
	private String streetTypeId;
	@Column(name="STREET_TYPE_NAME")
	private String streetTypeName;
	@Column(name="STREET_ID")
	private String streetId;
	
	@Column(name="STREET_NAME")
	private String streetName;
	
	@Column(name="N_HOUSE")
	private String nHouse;
	@Column(name="S_HOUSE")
	private String sHouse;
	@Column(name="CORPS")
	private String corps;
	@Column(name="N_FLAT")
	private String nFlat;
	@Column(name="S_FLAT")
	private String sFlat;
	@Column(name="POSTAL_CODE")
	private String postalCode;
	
}
