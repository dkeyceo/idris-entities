package com.dkey.idris.entities.domain.car;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDocPaperDomainPK  implements Serializable {

    private String docId;
    private String isNew;

}
