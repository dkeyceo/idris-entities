package com.dkey.idris.entities.domain.adp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "dai$adp_docs_process_status")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdpDocProcessStatus implements Serializable {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "doc#r")
    @JsonIgnore
    private AdpDoc adpDoc;

    @Column(name = "DATE_STATUS")
    private LocalDate dateStatus;
    @Column(name = "PROCESS_STATUS#R")
    private String processStatus;
    @Column(name = "RAW_DOC")
    private String rawDoc;
    @Column(name = "NOTE")
    private String note;
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)   
    @Column(name = "DOCSTS#")
    private Long docSts;

}
